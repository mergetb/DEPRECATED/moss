package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/containers/podman/v3/libpod/define"
	"github.com/containers/podman/v3/pkg/domain/entities"
	"github.com/containers/podman/v3/pkg/specgen"
	"github.com/spf13/viper"
	"gitlab.com/mergetb/tech/nex/pkg"
	"go.etcd.io/etcd/clientv3"
)

func openContainerFile(name string) (*os.File, error) {

	wd := viper.GetString("moss.workdir")
	containersDir := fmt.Sprintf("%s/containers", wd)
	tarfile := fmt.Sprintf("%s/%s.tar", containersDir, name)

	img, err := os.Open(tarfile)
	if err != nil {
		return nil, fmt.Errorf("error opening %s container archive: %v", name, err)
	}

	return img, nil

}

func checkHttpError(resp *http.Response) error {

	if resp.StatusCode >= 300 {

		// already exists
		if resp.StatusCode == http.StatusConflict {
			return nil
		}

		var msg string
		buf, err := ioutil.ReadAll(resp.Body)
		if err == nil {
			msg = string(buf)
		}

		if strings.Contains(msg, "already in use") {
			return nil
		}

		return fmt.Errorf("%s: %s", resp.Status, msg)
	}

	return nil

}

func loadContainer(host string, img *os.File) error {

	log.Printf("loading container on %s", host)

	resp, err := http.Post(
		fmt.Sprintf("http://%s:7474/v1.0.0/libpod/images/load", host),
		"application/x-tar",
		img,
	)
	if err != nil {
		return fmt.Errorf("container load error: %v", err)
	}

	return checkHttpError(resp)

}

func macgen() net.HardwareAddr {
	rand.Seed(time.Now().UnixNano())
	suffix := make([]byte, 3)
	rand.Read(suffix)
	return net.HardwareAddr{0x46, 0x76, 0xad, suffix[2], suffix[1], suffix[0]}
}

/** The basic model here is that at most one replica of a service may be running
* on a given host. Services may be replicated across hosts, but they may not be
* replicated on a single host at this time
**/

func getPodMac(name, host string) (net.HardwareAddr, error) {

	key := fmt.Sprintf("/mac/%s/%s", host, name)

	var mac net.HardwareAddr
	err := withEtcd(func(c *clientv3.Client) error {

		resp, err := c.Get(context.TODO(), key)
		if err != nil {
			return err
		}
		if len(resp.Kvs) == 0 {
			mac = macgen()
			_, err = c.Put(context.TODO(), key, mac.String())
			return err
		}
		mac, err = net.ParseMAC(string(resp.Kvs[0].Value))
		return err

	})

	return mac, err

}

func createPod(name, host string, ports ...specgen.PortMapping) error {

	nex.LoadConfig()

	// get a mac for the pod

	mac, err := getPodMac(name, host)

	// define the pod

	psg := specgen.PodSpecGenerator{}
	psg.Name = name
	psg.CNINetworks = []string{"moss"}
	psg.StaticMAC = &mac
	psg.PodCreateCommand = []string{
		"podman", "pod", "create", "--name", name,
	}
	psg.PortMappings = ports

	buf, err := json.Marshal(&psg)
	if err != nil {
		return fmt.Errorf("marhsal pod specgen: %v", err)
	}

	data := bytes.NewBuffer(buf)

	// create the pod

	resp, err := http.Post(
		fmt.Sprintf("http://%s:7474/v1.0.0/libpod/pods/create", host),
		"application/json",
		data,
	)
	if err != nil {
		return fmt.Errorf("create pod error: %v", err)
	}

	err = checkHttpError(resp)
	if err != nil {
		return err
	}

	// setup dns for the pod

	var otx nex.ObjectTx
	macindex := nex.NewMacIndex(&nex.Member{
		Mac:   mac.String(),
		Names: []string{fmt.Sprintf("%s.moss.net", name)},
	})

	err = nex.ReadNew(macindex)
	if err != nil {
		return err
	}
	if macindex.GetVersion() == 0 {
		// if the entry does not exist, create one
		otx.Put = append(otx.Put, macindex)

		for _, x := range nex.NewNameIndex(macindex.Member) {
			otx.Put = append(otx.Put, x)
		}

		err = nex.RunObjectTx(otx)
		if err != nil {
			return fmt.Errorf("nex transaction: %v", err)
		}
	}

	return nil

}

func createHostPod(name, host string, noSetManageHosts bool) error {

	nex.LoadConfig()

	// define the pod

	psg := specgen.PodSpecGenerator{}
	psg.Name = name
	if !noSetManageHosts {
		psg.Hostname = host
	}
	psg.NoManageHosts = noSetManageHosts
	psg.NetNS = specgen.Namespace{
		NSMode: specgen.Host,
	}
	psg.PodCreateCommand = []string{
		"podman", "pod", "create", "--name", name,
	}

	buf, err := json.Marshal(&psg)
	if err != nil {
		return fmt.Errorf("marhsal pod specgen: %v", err)
	}

	data := bytes.NewBuffer(buf)

	// create the pod

	resp, err := http.Post(
		fmt.Sprintf("http://%s:7474/v1.0.0/libpod/pods/create", host),
		"application/json",
		data,
	)
	if err != nil {
		return fmt.Errorf("create pod error: %v", err)
	}

	err = checkHttpError(resp)
	if err != nil {
		return err
	}

	return nil

}

func podStatus(name, host string) (entities.PodInspectReport, error) {

	pid := fmt.Sprintf("%s:%s", host, name)
	var pir entities.PodInspectReport

	resp, err := http.Get(
		fmt.Sprintf("http://%s:7474/v1.0.0/libpod/pods/%s/json", host, name),
	)
	if err != nil {
		return pir, fmt.Errorf("get pod %s: %v", pid, err)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return pir, fmt.Errorf("get pod %s read body: %v", pid, err)
	}

	if resp.StatusCode != http.StatusOK {
		return pir, fmt.Errorf("get pod %s failed: %d: %s: %s",
			pid, resp.StatusCode, resp.Status, string(data))
	}

	err = json.Unmarshal(data, &pir)
	if err != nil {
		return pir, fmt.Errorf("pod %s inspect unmarshal: %v", pid, err)
	}

	return pir, nil

}

func waitForPod(name, host string) (bool, string, error) {

	pid := fmt.Sprintf("%s:%s", host, name)

	pir, err := podStatus(name, host)
	if err != nil {
		return false, "", err
	}

	switch pir.State {
	case define.PodStateCreated:
		return false, pir.State, nil
	case define.PodStateErrored:
		return false, pir.State, fmt.Errorf("pod %s errored", pid)
	case define.PodStateExited:
		return false, pir.State, fmt.Errorf("pod %s exited", pid)
	case define.PodStatePaused:
		return false, pir.State, nil
	case define.PodStateRunning:
		return true, pir.State, nil
	case define.PodStateDegraded:
		return false, pir.State, fmt.Errorf("pod is %s degraded", pid)
	case define.PodStateStopped:
		return true, pir.State, nil
	default:
		return false, pir.State, fmt.Errorf("unknown pod state %s for %s", pir.State, pid)
	}

}

func createContainer(sg specgen.SpecGenerator, host string) error {

	sg.ContainerCreateCommand = []string{
		"podman", "continaer", "create", "--name", sg.Name, sg.Image}

	buf, err := json.Marshal(&sg)
	if err != nil {
		return fmt.Errorf("marshal spec generator: %v", err)
	}

	data := bytes.NewBuffer(buf)

	resp, err := http.Post(
		fmt.Sprintf("http://%s:7474/v1.0.0/libpod/containers/create", host),
		"application/json",
		data,
	)
	if err != nil {
		return fmt.Errorf("create pod error: %v", err)
	}

	return checkHttpError(resp)

}

func createPodService(pod, host string) error {

	resp, err := http.Get(
		fmt.Sprintf("http://%s:7474/v1.0.0/libpod/generate/%s/systemd?new=true&useName=true", host, pod),
	)
	if err != nil {
		return fmt.Errorf("create pod service: %v", err)
	}

	return checkHttpError(resp)

}
