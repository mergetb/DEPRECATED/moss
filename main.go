package main

import (
	"fmt"
	"log"
	"os"
	"text/tabwriter"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/mergetb/tech/nex/nex-server/nexserver"
)

var tw *tabwriter.Writer = tabwriter.NewWriter(os.Stdout, 0, 0, 4, ' ', 0)
var force bool

const (
	confdir  = "/etc/moss"
	conffile = confdir + "/moss.yml"
)

func main() {

	root := &cobra.Command{
		Use:   "moss",
		Short: "Merge OperationS Server",
	}

	run := &cobra.Command{
		Use:   "run",
		Short: "Run the Moss server",
		Run: func(*cobra.Command, []string) {
			os.Args = os.Args[0:1]
			run()
		},
	}
	root.AddCommand(run)

	fetch := &cobra.Command{
		Use:   "fetch",
		Short: "Fetch FCOS and iPXE artifacts",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			fetch()
		},
	}
	root.AddCommand(fetch)

	install := &cobra.Command{
		Use:   "install",
		Short: "Run the Moss server",
		Run: func(cmd *cobra.Command, args []string) {
			install(args)
		},
	}
	root.AddCommand(install)

	members := &cobra.Command{
		Use:   "members",
		Short: "List members",
		Run: func(*cobra.Command, []string) {
			listMembers()
		},
	}
	root.AddCommand(members)

	provision := &cobra.Command{
		Use:   "provision",
		Short: "Provision things",
	}
	root.AddCommand(provision)

	provisionServices := &cobra.Command{
		Use:   "services [<service> <service> ...]",
		Short: "Provision Mars services",
		Long:  "Provision Mars services. Empty list provisions all services.",
		Run: func(cmd *cobra.Command, args []string) {
			provisionServices(args)
		},
	}
	provision.AddCommand(provisionServices)

	provisionHarbor := &cobra.Command{
		Use:   "harbor",
		Short: "Provision Mars harbor",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := provisionHarbor()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	provision.AddCommand(provisionHarbor)

	provisionMinio := &cobra.Command{
		Use:   "minio",
		Short: "Provision MinIO with the basics",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := provisionMinio()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	provision.AddCommand(provisionMinio)

	provisionImages := &cobra.Command{
		Use:   "images",
		Short: "Add base images to MinIO",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := provisionImages()
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	provision.AddCommand(provisionImages)

	provisionHosts := &cobra.Command{
		Use:   "hosts [<host>, <host> ...]",
		Short: "Provision Mars hosts",
		Long:  "Provision Mars hosts. Empty list provisions all hosts.",
		Run: func(cmd *cobra.Command, args []string) {
			provisionHosts(args)
		},
	}
	provisionHosts.Flags().BoolVarP(&force, "force", "f", false, "force provision")
	provision.AddCommand(provisionHosts)

	var modelfile string
	provisionModel := &cobra.Command{
		Use:   "model",
		Short: "Add testbed model to MinIO",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			err := provisionModel(modelfile)
			if err != nil {
				log.Fatal(err)
			}
		},
	}
	provisionModel.Flags().StringVarP(
		&modelfile, "model", "m", "/etc/tbxir.pbuf", "XIR model to provision")
	provision.AddCommand(provisionModel)

	upgrade := &cobra.Command{
		Use:   "upgrade [<service>, <service> ...]",
		Short: "Upgrade Mars services",
		Long:  "Upgrade Mars services. Empty list upgrades all services.",
		Run: func(cmd *cobra.Command, args []string) {
			upgrade(args)
		},
	}
	root.AddCommand(upgrade)

	nat := &cobra.Command{
		Use:   "nat",
		Short: "Setup a nat <interface>",
		Long:  "Setup an nftables based NAT and ip forwarding",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			setupNat(args[0])
		},
	}
	root.AddCommand(nat)

	name := &cobra.Command{
		Use:   "name <mac> <name> [<name> ...]",
		Short: "Associate a MAC with a set of names",
		Args:  cobra.MinimumNArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setName(args[0], args[1:])
		},
	}
	root.AddCommand(name)

	hostname := &cobra.Command{
		Use:   "hostname <mac> <hostname>",
		Short: "Associate a MAC with a set of names",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			setHostName(args[0], args[1])
		},
	}
	root.AddCommand(hostname)

	status := &cobra.Command{
		Use:   "status",
		Short: "Get testbed operations status",
		Args:  cobra.NoArgs,
		Run: func(*cobra.Command, []string) {
			showStatus()
		},
	}
	root.AddCommand(status)

	option(root, "workdir", "/var/moss", "working directory")
	option(root, "fcos-version", "33.20210314.3.0", "Fedora CoreOS version")
	option(root, "ipxe-version", "1161485410", "iPXE Version")
	option(root, "mars-version", "1165112175", "Mars Version")
	option(root, "nex-version", "1145138753", "Nex Version")
	option(root, "default-netdev", "eth0", "Default netdev to use for provisioning")

	nexserver.BindFlags(root)
	viper.Set("nex.dns.domain", "moss.net")

	root.Execute()

}

func run() {

	viper.SetConfigName("moss.yml")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(confdir)
	viper.ReadInConfig()

	wkd := viper.GetString("moss.workdir")

	os.MkdirAll(fmt.Sprintf("%s/tftp", wkd), 0755)
	os.MkdirAll(fmt.Sprintf("%s/fcos", wkd), 0755)
	os.MkdirAll(fmt.Sprintf("%s/spore", wkd), 0755)
	os.MkdirAll(fmt.Sprintf("%s/nex", wkd), 0755)
	os.MkdirAll(fmt.Sprintf("%s/ssh", wkd), 0755)

	certinit()
	fetch()
	installKey()

	runNex()
	go runhttp()
	runtftp()

}

func option(cmd *cobra.Command, name, defval, desc string) {

	opt := fmt.Sprintf("moss.%s", name)

	cmd.PersistentFlags().String(opt, defval, desc)
	viper.BindPFlag(opt, cmd.PersistentFlags().Lookup(opt))

}
