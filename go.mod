module gitlab.com/mergetb/ops/moss

go 1.15

require (
	github.com/containernetworking/cni v0.8.1
	github.com/containers/image/v5 v5.10.2
	github.com/containers/podman/v3 v3.1.0-rc1
	github.com/coreos/go-systemd/v22 v22.1.0
	github.com/coreos/ignition/v2 v2.9.0
	github.com/golang/protobuf v1.4.3
	github.com/gorilla/handlers v1.5.1
	github.com/labstack/echo/v4 v4.1.17
	github.com/lorenzosaino/go-sysctl v0.1.1
	github.com/minio/minio v0.0.0-20210218231620-f28b06309116
	github.com/minio/minio-go v3.0.2+incompatible // indirect
	github.com/minio/minio-go/v7 v7.0.9-0.20210210235136-83423dddb072
	github.com/opencontainers/runtime-spec v1.0.3-0.20200817204227-f9c09b4ea1df
	github.com/pin/tftp v2.1.0+incompatible
	github.com/sethvargo/go-password v0.2.0
	github.com/sirupsen/logrus v1.8.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.1
	github.com/takama/daemon v1.0.0
	gitlab.com/mergetb/portal/services v0.5.18-0.20210330221820-7948a00ea77d
	gitlab.com/mergetb/tech/mars v0.0.0-20210331033804-2824cdf7173a
	gitlab.com/mergetb/tech/nex v0.5.8-0.20210228201946-65e66a14a78e
	gitlab.com/mergetb/xir v0.2.20-0.20210330020237-46e831f3bc35
	go.etcd.io/etcd v0.5.0-alpha.5.0.20200306183522-221f0cc107cb
	go.universe.tf/netboot v0.0.0-20201124111825-bdaec9d82638
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	google.golang.org/grpc v1.33.2
	google.golang.org/protobuf v1.25.0
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)

replace (
	google.golang.org/grpc => google.golang.org/grpc v1.27.0
	google.golang.org/grpc/cmd/protoc-gen-go-grpc => google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.0.0
	k8s.io/api => k8s.io/api v0.18.2
	k8s.io/apimachinery => k8s.io/apimachinery v0.18.3-beta.0
	k8s.io/client-go => k8s.io/client-go v0.18.2
)
