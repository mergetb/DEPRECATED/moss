package main

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/portal/services/api"
	"gitlab.com/mergetb/tech/mars/api"
	"gitlab.com/mergetb/xir/v0.3/go"
)

func provisionHarbor() error {

	log.Printf("provisoning harbor")

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	mz := &portal.Materialization{
		Pid: "marstb",
		Eid: "system",
		Rid: "harbor",
	}

	// Determine what hosts need to be on the harbor
	m := make(map[string]bool)
	for _, host := range mf.Services.Nex {
		m[host] = true
	}
	for _, host := range mf.Services.Tftp {
		m[host] = true
	}

	harborSeg := new(portal.LinkSegment)
	lrz := &portal.LinkRealization{
		Link: &xir.Link{Id: "harbor"},
		Segments: map[uint64]*portal.LinkSegment{
			3: harborSeg,
		},
	}
	for host := range m {

		h, ok := mf.Hosts[host]
		if !ok {
			return fmt.Errorf("%s has no host config", h)
		}

		harborSeg.Endpoints = append(harborSeg.Endpoints, &portal.Endpoint{
			Host: host,
			Interface: &portal.Endpoint_Vtep{
				&portal.Vtep{
					Name:     "vtep3",
					Vni:      3,
					TunnelIp: h.Harbor.TunnelIp,
					//ServiceIp: h.Harbor.Address,
					Parent: h.Harbor.Netdev,
				},
			},
		})

	}

	mz.Links = append(mz.Links, &portal.Link{
		Realization: lrz,
	})

	conn, cli, err := apiserverClient()
	if err != nil {
		return fmt.Errorf("harbor client: %v", err)
	}
	defer conn.Close()

	_, err = cli.Materialize(context.TODO(), &mars.MaterializeRequest{
		Materialization: mz,
	})
	if err != nil {
		return fmt.Errorf("harbor materialize: %v", err)
	}

	return nil

}

func apiserverClient() (*grpc.ClientConn, mars.MarsClient, error) {

	creds := credentials.NewTLS(&tls.Config{
		//XXX XXX XXX
		InsecureSkipVerify: true,
	})
	conn, err := grpc.Dial("apiserver:6001",
		grpc.WithTransportCredentials(creds),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("apiserver dial: %v", err)
	}

	return conn, mars.NewMarsClient(conn), nil

}
