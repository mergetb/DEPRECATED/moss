package main

type IpxeParams struct {
	Id          string
	InstallDisk string
	Kernel      string
	Rootfs      string
	Initramfs   string
	Netdev      string
	Mac         string
	Server      string
}

var ipxeTemplate = `#!ipxe
dhcp
kernel http://{{.Server}}/{{.Kernel}} initrd=initramfs coreos.live.rootfs_url=http://{{.Server}}/{{.Rootfs}} coreos.inst.install_dev={{.InstallDisk}} coreos.inst.ignition_url=http://{{.Server}}/ignition ip={{.Netdev}}:dhcp selinux=0
initrd http://{{.Server}}/{{.Initramfs}}
boot`

type PodmanServiceParams struct {
	Netdev string
	Mac    string
}

var podmanServiceEnabled = true
var podmanServiceTemplate = `[Unit]
Description=Podman API Service
After=network-online.target
Wants=network-online.target

[Service]
#ExecStart=/usr/bin/bash -c "podman system service -t 0 tcp:$(ip -j addr show dev {{.Netdev}} | jq -r '.[0].addr_info[0].local'):7474"
ExecStart=/usr/bin/bash -c "podman system service -t 0 tcp:$(ip -j addr | jq -r '.[] | select (.address==\"{{.Mac}}\").addr_info | .[] | select(.family==\"inet\").local'):7474"

[Install]
WantedBy=multi-user.target
`

var podmanLocalServiceTemplate = `[Unit]
Description=Podman API Service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/bin/podman system service -t 0 unix:///var/run/podman/podman.sock

[Install]
WantedBy=multi-user.target
`

var sporeServiceEnabled = true
var sporeServiceTemplate = `[Unit]
Description=Moss Spore Service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/local/bin/spore

[Install]
WantedBy=multi-user.target
`

type NftablesParams struct {
	Ifx string
}

var nftablesTemplate = `table ip nat {
        chain prerouting {
                type nat hook prerouting priority dstnat; policy accept;
        }

        chain postrouting {
                type nat hook postrouting priority srcnat; policy accept;
                oifname "{{.Ifx}}" masquerade
        }
}
`

var sysctlTemplate = `net.ipv4.ip_forward=1
`

var podmanDhcpEnabled = true

// https://www.redhat.com/sysadmin/leasing-ips-podman
var podmanDhcpSocketTemplate = `[Unit]
Description=DHCP Client for CNI

[Socket]
ListenStream=%t/cni/dhcp.sock
SocketMode=0600

[Install]
WantedBy=sockets.target
`

var podmanDhcpServiceTemplate = `[Unit]
Description=DHCP Client CNI Service
Requires=io.podman.dhcp.socket
After=io.podman.dhcp.socket

[Service]
Type=simple
ExecStart=/usr/libexec/cni/dhcp daemon
TimeoutStopSec=30
KillMode=process

[Install]
WantedBy=multi-user.target
Also=io.podman.dhcp.socket
`

var frrDaemons = `bgpd=yes
ospfd=no
ospf6d=no
ripd=no
ripngd=no
isisd=no
pimd=no
ldpd=no
nhrpd=no
eigrpd=no
babeld=no
sharpd=no
pbrd=no
bfdd=no
fabricd=no
vrrpd=no

vtysh_enable=yes
zebra_options="  -A 127.0.0.1 -s 90000000"
bgpd_options="   -A 127.0.0.1"
ospfd_options="  -A 127.0.0.1"
ospf6d_options=" -A ::1"
ripd_options="   -A 127.0.0.1"
ripngd_options=" -A ::1"
isisd_options="  -A 127.0.0.1"
pimd_options="   -A 127.0.0.1"
ldpd_options="   -A 127.0.0.1"
nhrpd_options="  -A 127.0.0.1"
eigrpd_options=" -A 127.0.0.1"
babeld_options=" -A 127.0.0.1"
sharpd_options=" -A 127.0.0.1"
pbrd_options="   -A 127.0.0.1"
staticd_options="-A 127.0.0.1"
bfdd_options="   -A 127.0.0.1"
fabricd_options="-A 127.0.0.1"
vrrpd_options="  -A 127.0.0.1"


frr_profile="datacenter"
`

var frrConf = `log stdout
`

var vtyshConf = `service integrated-vtysh-config
`

type ZtpParams struct {
	Pubkey        string
	License       string
	TLSCert       string
	TLSKey        string
	MinioPassword string
}

var ztpTemplate = `#!/bin/bash
function error() {
  echo -e "ERROR: The ZTP script failed while running the command $BASH_COMMAND at line $BASH_LINENO." >&2
  exit 1
}

# Log all output from this script
exec >> /var/log/autoprovision 2>&1
date "+%FT%T ztp starting script $0"

trap error ERR

mkdir -p /root/.ssh
echo {{.Pubkey}} >> /root/.ssh/authorized_keys
chmod 0600 /root/.ssh/authorized_keys

{{if .License}}
echo {{.License}} > /etc/license.txt
/usr/cumulus/bin/cl-license -i /etc/license.txt && systemctl restart switchd.service
{{end}}

mkdir -p /certs

cat << EOF > /certs/apiserver.pem
{{.TLSCert}}
EOF

cat << EOF > /certs/apiserver-key.pem
{{.TLSKey}}
EOF

### Canopy

systemctl stop canopy || true

curl http://moss/canopy -o /usr/local/bin/canopy
chmod +x /usr/local/bin/canopy

cat << EOF > /lib/systemd/system/canopy@.service
[Unit]
Description=Canopy service
After=network-online.target
Wants=network-online.target

[Service]
ExecStart=/usr/local/bin/canopy
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
EOF

grep -qxF 'canopy' /etc/vrf/systemd.conf || echo 'canopy' >> /etc/vrf/systemd.conf

systemctl daemon-reload
systemctl enable canopy@mgmt
systemctl start canopy@mgmt

sed -i 's/^bgpd=no/bgpd=yes/g' /etc/frr/daemons
systemctl restart frr

### Dance

curl http://moss/dance -o /usr/local/bin/dance
chmod +x /usr/local/bin/dance

cat << EOF > /lib/systemd/system/dance@.service
[Unit]
Description=Dance service
After=network-online.target
Wants=network-online.target

[Service]
Environment=MINIO_ROOT_USER=mars
Environment=MINIO_ROOT_PASSWORD={{.MinioPassword}}
ExecStart=/usr/local/bin/dance
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target
EOF

grep -qxF 'dance' /etc/vrf/systemd.conf || echo 'dance' >> /etc/vrf/systemd.conf

systemctl daemon-reload
systemctl enable dance@mgmt
systemctl start dance@mgmt

# CUMULUS-AUTOPROVISIONING
exit 0
`

var noNmImperialism = `[main]
no-auto-default=*
`

type MgmtBrParams struct {
	Mac string
}

var mgmtBrTemplate = `[connection]
id=bridge
type=bridge
interface-name=bridge
[bridge]
mac-address={{.Mac}}
[ipv4]
dns-search=
may-fail=false
method=auto
`

type MgmtIfxParams struct {
	Netdev string
}

var mgmtIfxTemplate = `[connection]
id=bridge-{{.Netdev}}-member
type=ethernet
interface-name={{.Netdev}}
master=bridge
slave-type=bridge
[bridge-port]
`
