package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	cp "github.com/containers/image/v5/copy"
	"github.com/containers/image/v5/docker"
	"github.com/containers/image/v5/docker/archive"
	"github.com/containers/image/v5/signature"
	"github.com/containers/podman/v3/pkg/specgen"
	"github.com/minio/minio-go/v7"
	spec "github.com/opencontainers/runtime-spec/specs-go"
	"github.com/sethvargo/go-password/password"
	"github.com/spf13/viper"
	"gitlab.com/mergetb/tech/nex/pkg"
	"go.etcd.io/etcd/clientv3"
	"google.golang.org/grpc"
)

var hostProvisioners = map[string][]func(){
	"etcd": []func(){
		provisionHostname,
		provisionEtcd,
	},
	"minio": []func(){
		provisionHostname,
		provisionMinIO,
	},
	"ceph": []func(){
		provisionHostname,
		provisionCeph,
	},
	"infraserver": []func(){
		provisionHostname,
		provisionInfraserver,
	},
}

func hostProvisionerList() []string {
	var ls []string
	for x := range hostProvisioners {
		ls = append(ls, x)
	}
	return ls
}

var serviceProvisioners = map[string]func(){
	"apiserver": provisionApiserver,
	"frr":       provisionFrr,
	"infranet":  provisionInfranet,
	"infrapod":  provisionInfrapod,
	"sled":      provisionSled,
	"rally":     provisionRally,
	"metal":     provisionMetal,
	"canopy":    provisionCanopy,
	"nex":       provisionNex,
	"tftp":      provisionTftp,
}

func serviceProvisionerList() []string {
	var ls []string
	for x := range serviceProvisioners {
		ls = append(ls, x)
	}
	return ls
}

func provisionServices(args []string) {

	if len(args) > 0 {
		// if the caller has asked for a specific set of provisioners use those
		for _, arg := range args {
			provisioner, ok := serviceProvisioners[arg]
			if !ok {
				log.Fatalf(
					"%s is not a recognized service, must be one of %s",
					arg,
					strings.Join(serviceProvisionerList(), " "),
				)
			}
			provisioner()
		}
	} else {
		// otherwise run all provisioners
		for _, provisioner := range serviceProvisioners {
			provisioner()
		}
	}

}

func provisionHosts(args []string) {

	if len(args) > 0 {
		// if the caller has asked for a specific set of provisioners use those
		for _, arg := range args {
			provisioners, ok := hostProvisioners[arg]
			if !ok {
				log.Fatalf(
					"%s is not a recognized host, must be one of %s",
					arg,
					strings.Join(hostProvisionerList(), " "),
				)
			}
			for _, provisioner := range provisioners {
				provisioner()
			}
		}
	} else {
		// otherwise run all provisioners
		for _, provisioners := range hostProvisioners {
			for _, provisioner := range provisioners {
				provisioner()
			}
		}
	}

}

func provisionHostname() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	for host, hostInfo := range mf.Hosts {
		if hostInfo.Kind != KindFCOS {
			continue
		}
		err := setSporeHostname(host)
		if err != nil {
			log.Fatal(err)
		}
	}

}

func provisionApiserver() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	gconf, err := readGenConf()
	if err != nil {
		log.Fatal(err)
	}

	if len(mf.Services.Apiserver) == 0 {
		// nothing to do
		return
	}

	prepareCIArtifactContainer(marsCIArtifactURI("apiserver"), "apiserver")
	img, err := openContainerFile("apiserver")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, host := range mf.Services.Apiserver {

		log.Printf("provisioning api server on %s", host)

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("apiserver", host, false)
		if err != nil {
			log.Fatalf("create pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "server"
		sg.Pod = "apiserver"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/apiserver:latest"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Destination: "/certs/apiserver.pem",
				Type:        "bind",
				Source:      "/var/lib/spore/cert.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Destination: "/certs/apiserver-key.pem",
				Type:        "bind",
				Source:      "/var/lib/spore/key.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
		}
		sg.Env = map[string]string{
			"MINIO_ROOT_USER":     "mars",
			"MINIO_ROOT_PASSWORD": gconf.MinioPassword,
		}
		err = createContainer(sg, host)

		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("apiserver", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func provisionFrr() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	uri := "quay.io/mergetb/frr:solovni"
	prepareRegistryContainer(uri, "frr")
	frrimg, err := openContainerFile("frr")
	if err != nil {
		log.Fatal(err)
	}
	defer frrimg.Close()

	for _, host := range mf.Services.Frr {

		log.Printf("provisioning frr server on %s", host)

		frrid := uint32(92)

		err = addSporeDir(host, "/var/run/frr", frrid, frrid, 0755)
		if err != nil {
			log.Fatal(err)
		}

		err = addSporeFile(
			host, "/var/lib/volumes/frr/daemons", frrid, frrid, 0600, []byte(frrDaemons))
		if err != nil {
			log.Fatal(err)
		}

		err = addSporeFile(
			host, "/var/lib/volumes/frr/frr.conf", frrid, frrid, 0600, []byte(frrConf))
		if err != nil {
			log.Fatal(err)
		}
		err = addSporeFile(
			host, "/var/lib/volumes/frr/vtysh.conf", frrid, frrid, 0600, []byte(vtyshConf))
		if err != nil {
			log.Fatal(err)
		}

		err = loadContainer(host, frrimg)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("frr", host, false)
		if err != nil {
			log.Fatal(err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "frr-router"
		sg.Pod = "frr"
		sg.Image = "mergetb.io/mars/frr:latest"
		sg.Privileged = true
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Type:        "bind",
				Source:      "/var/lib/volumes/frr",
				Destination: "/etc/frr",
				Options:     []string{"rbind", "rw", "z"},
			},
			{
				Type:        "bind",
				Source:      "/var/run/frr",
				Destination: "/var/run/frr",
				Options:     []string{"rbind", "rw", "z"},
			},
		}
		err = createContainer(sg, host)
		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("frr", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func provisionNex() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	if len(mf.Services.Nex) == 0 {
		return
	}

	prepareCIArtifactContainer(
		fmt.Sprintf(
			"https://gitlab.com/mergetb/tech/nex/-/jobs/%s/artifacts/raw/build/nex.tar",
			viper.GetString("moss.nex-version"),
		),
		"nex",
	)
	img, err := openContainerFile("nex")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, host := range mf.Services.Nex {

		log.Printf("provisioning nex on %s", host)

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("nex", host, false)
		if err != nil {
			log.Fatalf("create pod: %v", err)
		}

		args := []string{
			"--nex.domain", "sled.marstb.net",
			"--nex.interface", "vtep3",
			"--nex.listen", "0.0.0.0:6003", // TODO grab actual address for host from moss' nex
			"--nex.etcd-server", "etcd",
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "nex-server"
		sg.Pod = "nex"
		sg.Image = "registry.gitlab.com/mergetb/tech/nex/nex:latest"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Env = map[string]string{
			"ARGS": strings.Join(args, " "),
		}
		sg.CapAdd = []string{
			"CAP_NET_BIND_SERVICE",
			"CAP_NET_RAW",
		}

		err = createContainer(sg, host)

		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("nex", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

	for {
		ok, state, err := waitForPod("nex", mf.Services.Nex[0])
		if err != nil {
			log.Fatal(err)
		}
		if ok {
			break
		}
		log.Printf("wating for %s:nex to come up, currently %s",
			mf.Services.Nex[0], state,
		)
		time.Sleep(time.Second * 1)
	}

	err = configureNex(mf.Services.Nex[0])
	if err != nil {
		log.Fatal(err)
	}

}

func provisionTftp() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	if len(mf.Services.Tftp) == 0 {
		return
	}

	prepareCIArtifactContainer(marsCIArtifactURI("tftp"), "tftp")
	img, err := openContainerFile("tftp")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	fetchIPXE("tmp", "1161485410", "sled")

	ipxeEFI, err := ioutil.ReadFile("/tmp/tftp/snponly.efi")
	if err != nil {
		log.Fatalf("read ipxe efi: %v", err)
	}

	ipxeBIOS, err := ioutil.ReadFile("/tmp/tftp/ipxe.pxe")
	if err != nil {
		log.Fatalf("read ipxe bios: %v", err)
	}

	for _, host := range mf.Services.Tftp {

		log.Printf("provisioning tftp on %s", host)

		err = addSporeFile(host, "/srv/tftp/snponly.efi", 0, 0, 0644, ipxeEFI)
		if err != nil {
			log.Fatalf("add spore ipxe efi: %v", err)
		}

		err = addSporeFile(host, "/srv/tftp/ipxe.bios", 0, 0, 0644, ipxeBIOS)
		if err != nil {
			log.Fatalf("add spore bios efi: %v", err)
		}

		err = loadContainer(host, img)
		if err != nil {
			log.Fatalf("load tftp container: %v", err)
		}

		err = createHostPod("tftp", host, false)
		if err != nil {
			log.Fatalf("create tftp pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "tftp-server"
		sg.Pod = "tftp"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/tftp:latest"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Type:        "bind",
				Destination: "/srv/tftp",
				Source:      "/srv/tftp",
				Options:     []string{"rbind", "ro", "z"},
			},
		}

		sg.CapAdd = []string{
			"CAP_NET_BIND_SERVICE",
		}

		err = createContainer(sg, host)

		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("tftp", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func configureNex(host string) error {

	conn, nexc, err := nexClient(host)
	if err != nil {
		return fmt.Errorf("nex client: %v", err)
	}
	defer conn.Close()

	resp, err := nexc.GetNetworks(context.TODO(), &nex.GetNetworksRequest{})
	if err != nil {
		return fmt.Errorf("nex get networks: %v", err)
	}

	for _, n := range resp.Nets {
		if n == "sled" {
			// nothing to do, harbor network exists
			return nil
		}
	}

	// image network

	n := &nex.Network{
		Name:        "sled",
		Subnet4:     "172.29.0.0/16",
		Gateways:    []string{"172.29.0.1"},
		Nameservers: []string{"172.29.0.1"},
		Dhcp4Server: "172.29.0.1",
		Domain:      "sled.marstb.net",
		MacRange: &nex.AddressRange{
			Begin: "00:00:00:00:00:00",
			End:   "ff:ff:ff:ff:ff:ff",
		},
		Range4: &nex.AddressRange{
			Begin: "172.29.0.100",
			End:   "172.29.254.254",
		},
		Options: []*nex.Option{
			{Number: 66, Value: "sled-pxe"},
			{Number: 67, Value: "snponly.efi"},
		},
	}

	_, err = nexc.AddNetwork(context.TODO(), &nex.AddNetworkRequest{Network: n})
	if err != nil {
		return fmt.Errorf("nex add network: %v", err)
	}

	// static service network

	n = &nex.Network{
		Name:        "sled-static",
		Subnet4:     "172.29.0.0/16",
		Gateways:    []string{"172.29.0.1"},
		Nameservers: []string{"172.29.0.1"},
		Dhcp4Server: "172.29.0.1",
		Domain:      "sled.marstb.net",
	}

	_, err = nexc.AddNetwork(context.TODO(), &nex.AddNetworkRequest{Network: n})
	if err != nil {
		return fmt.Errorf("nex add network: %v", err)
	}

	return nil

}

func nexClient(host string) (*grpc.ClientConn, nex.NexClient, error) {

	conn, err := grpc.Dial(fmt.Sprintf("%s:6003", host), grpc.WithInsecure())
	if err != nil {
		return nil, nil, err
	}

	return conn, nex.NewNexClient(conn), nil

}

func provisionSled() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	prepareCIArtifactContainer(marsCIArtifactURI("sled"), "sled")
	img, err := openContainerFile("sled")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	wd := viper.GetString("moss.workdir")

	sledKernel, err := ioutil.ReadFile(fmt.Sprintf("/%s/sled/kernel", wd))
	if err != nil {
		log.Fatalf("read sled kernel: %v", err)
	}

	sledInitramfs, err := ioutil.ReadFile(fmt.Sprintf("/%s/sled/initramfs", wd))
	if err != nil {
		log.Fatalf("read sled initramfs: %v", err)
	}

	for _, host := range mf.Services.Sled {

		log.Printf("provisioning sled server on %s", host)

		err = addSporeFile(host, "/srv/sled/kernel", 0, 0, 0644, sledKernel)
		if err != nil {
			log.Fatalf("add spore sled kernel: %v", err)
		}

		err = addSporeFile(host, "/srv/sled/initramfs", 0, 0, 0644, sledInitramfs)
		if err != nil {
			log.Fatalf("add spore sled initramfs: %v", err)
		}

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("sled", host, false)
		if err != nil {
			log.Fatal(err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "sled-server"
		sg.Pod = "sled"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/sled:latest"
		sg.Privileged = true
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Type:        "bind",
				Source:      "/srv/sled",
				Destination: "/srv/sled",
				Options:     []string{"rbind", "ro", "z"},
			},
		}
		err = createContainer(sg, host)
		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("sled", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func provisionMetal() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	gconf, err := readGenConf()
	if err != nil {
		log.Fatal(err)
	}

	if len(mf.Services.Metal) == 0 {
		// nothing to do
		return
	}

	prepareCIArtifactContainer(marsCIArtifactURI("metal"), "metal")
	img, err := openContainerFile("metal")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, host := range mf.Services.Metal {

		log.Printf("provisioning metal service on %s", host)

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("metal", host, false)
		if err != nil {
			log.Fatalf("create pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "metal-server"
		sg.Pod = "metal"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/metal:latest"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Destination: "/certs/apiserver.pem",
				Type:        "bind",
				Source:      "/var/lib/spore/cert.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Destination: "/certs/apiserver-key.pem",
				Type:        "bind",
				Source:      "/var/lib/spore/key.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
		}
		sg.Env = map[string]string{
			"MINIO_ROOT_USER":     "mars",
			"MINIO_ROOT_PASSWORD": gconf.MinioPassword,
		}

		err = createContainer(sg, host)
		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("metal", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func provisionInfrapod() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	if len(mf.Services.Infrapod) == 0 {
		// nothing to do
		return
	}

	prepareCIArtifactContainer(marsCIArtifactURI("infrapod"), "infrapod")
	img, err := openContainerFile("infrapod")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, host := range mf.Services.Infrapod {

		log.Printf("provisioning infrapod service on %s", host)

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("infrapod", host, true)
		if err != nil {
			log.Fatalf("create pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "infrapod-server"
		sg.Pod = "infrapod"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/infrapod:latest"
		sg.UseImageHosts = true
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Destination: "/certs/apiserver.pem",
				Type:        "bind",
				Source:      "/var/lib/spore/cert.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Destination: "/certs/apiserver-key.pem",
				Type:        "bind",
				Source:      "/var/lib/spore/key.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
		}

		err = createContainer(sg, host)
		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("infrapod", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func provisionInfraserver() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	prepareCIArtifactContainer(marsCIArtifactURI("cniman"), "cniman")
	img, err := openContainerFile("cniman")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	prepareCIArtifactContainer(
		fmt.Sprintf(
			"https://gitlab.com/mergetb/tech/nex/-/jobs/%s/artifacts/raw/build/nex.tar",
			viper.GetString("moss.nex-version"),
		),
		"nex",
	)
	neximg, err := openContainerFile("nex")
	if err != nil {
		log.Fatal(err)
	}
	defer neximg.Close()

	for _, host := range mf.Infrastructure.Infraserver {

		log.Printf("provisioing cniman service on %s", host)

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = loadContainer(host, neximg)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("cniman", host, false)
		if err != nil {
			log.Fatalf("create pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "cniman-server"
		sg.Pod = "cniman"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/cniman:latest"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Source:      "/var/lib/spore/cert.pem",
				Destination: "/certs/apiserver.pem",
				Type:        "bind",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Source:      "/var/lib/spore/key.pem",
				Destination: "/certs/apiserver-key.pem",
				Type:        "bind",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Source:      "/etc/cni/net.d/",
				Destination: "/etc/cni/net.d/",
				Type:        "bind",
				Options:     []string{"rbind", "rw", "z"},
			},
		}

		err = createContainer(sg, host)
		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("cniman", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

// TODO
func provisionInfranet() {}
func provisionRally()    {}
func provisionCanopy() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	prepareCIArtifactContainer(marsCIArtifactURI("canopy"), "canopy")
	img, err := openContainerFile("canopy")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, host := range mf.Services.Canopy {

		hostinfo, ok := mf.Hosts[host]
		if !ok || hostinfo.Kind != KindFCOS {
			continue
		}

		err = loadContainer(host, img)
		if err != nil {
			log.Fatal(err)
		}

		err = createHostPod("canopy", host, false)
		if err != nil {
			log.Fatal(err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "canopy-server"
		sg.Pod = "canopy"
		sg.Image = "registry.gitlab.com/mergetb/tech/mars/canopy:latest"
		sg.Privileged = true
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Mounts = []spec.Mount{
			{
				Type:        "bind",
				Source:      "/var/lib/spore/cert.pem",
				Destination: "/certs/apiserver.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Type:        "bind",
				Source:      "/var/lib/spore/key.pem",
				Destination: "/certs/apiserver-key.pem",
				Options:     []string{"rbind", "ro", "z"},
			},
			{
				Type:        "bind",
				Source:      "/var/run/frr",
				Destination: "/var/run/frr",
				Options:     []string{"rbind", "rw", "z"},
			},
		}
		err = createContainer(sg, host)
		if err != nil {
			log.Fatalf("create container: %v", err)
		}

		err = addSporeService("canopy", host)
		if err != nil {
			log.Fatalf("add spore service: %v", err)
		}

	}

}

func provisionEtcd() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	uri := "quay.io/coreos/etcd:v3.4.14"
	prepareRegistryContainer(uri, "etcd")
	img, err := openContainerFile("etcd")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, x := range mf.Infrastructure.Etcd {

		// ensure the disk is set up

		provisionDisk(x.Host, x.Disk, "etcd")

		// load the etcd image onto the host

		err = loadContainer(x.Host, img)
		if err != nil {
			log.Fatalf("load etcd container: %v", err)
		}

		// create the pod

		err = createHostPod("etcd", x.Host, false)
		if err != nil {
			log.Fatal("create etcd pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "etcd-server"
		sg.Pod = "etcd"
		sg.Image = "mergetb.io/mars/etcd"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}

		sg.Mounts = []spec.Mount{
			{
				Type:        "bind",
				Destination: "/default.etcd",
				Source:      "/var/mnt/etcd",
				Options:     []string{"rbind", "rw", "z"},
			},
		}
		sg.Env = map[string]string{
			"ETCD_LISTEN_CLIENT_URLS":    "http://0.0.0.0:2379",
			"ETCD_ADVERTISE_CLIENT_URLS": "http://etcd:2379",
		}
		err = createContainer(sg, x.Host)
		if err != nil {
			log.Fatalf("create etcd container: %v", err)
		}

		// launch the pod

		err = addSporeService("etcd", x.Host)
		if err != nil {
			log.Fatalf("add spore etcd service: %v", err)
		}

	}

}

func marsCIArtifactURI(name string) string {

	return fmt.Sprintf(
		"https://gitlab.com/mergetb/tech/mars/-/jobs/%s/artifacts/raw/build/%s.tar",
		viper.GetString("moss.mars-version"),
		name,
	)

}

//TODO should just pull from gitlab registry instead.
func prepareCIArtifactContainer(url, name string) {

	wd := viper.GetString("moss.workdir")
	containersDir := fmt.Sprintf("%s/containers", wd)
	tarfile := fmt.Sprintf("%s/%s.tar", containersDir, name)

	_, err := os.Stat(tarfile)
	if err != nil {
		err = os.MkdirAll(containersDir, 0755)
		if err != nil {
			log.Fatalf("ensure contianers dir: %v", err)
		}
		log.Printf("%s image not found, attempting to pull from CI", name)
		fetchFile(url, tarfile)
	}
}

func prepareRegistryContainer(url, name string) {

	wd := viper.GetString("moss.workdir")
	containersDir := fmt.Sprintf("%s/containers", wd)
	tarfile := fmt.Sprintf("%s/%s.tar", containersDir, name)

	_, err := os.Stat(tarfile)
	if err != nil {

		// pull container if we do not have it already

		err = os.MkdirAll(containersDir, 0755)
		if err != nil {
			log.Fatalf("ensure contianers dir: %v", err)
		}

		log.Printf("%s image not found, attempting to pull", name)

		src, err := docker.Transport.ParseReference(fmt.Sprintf("//%s", url))
		if err != nil {
			log.Fatalf("parse source container ref (%s): %v", url, err)
		}

		dest, err := archive.Transport.ParseReference(
			fmt.Sprintf("%s:mergetb.io/mars/%s", tarfile, name),
		)
		if err != nil {
			log.Fatalf("parse destination container ref (%s): %v", tarfile, err)
		}

		opts := &cp.Options{
			//ImageListSelection: cp.CopyAllImages,
		}

		// default fedora policy
		policy := &signature.Policy{
			Default: signature.PolicyRequirements{
				signature.NewPRInsecureAcceptAnything(),
			},
			Transports: map[string]signature.PolicyTransportScopes{
				"docker-daemon": map[string]signature.PolicyRequirements{
					"": signature.PolicyRequirements{
						signature.NewPRInsecureAcceptAnything(),
					},
				},
			},
		}
		//policy, err := signature.DefaultPolicy(&types.SystemContext{})
		if err != nil {
			log.Fatalf("default policy: %v", err)
		}

		pc, err := signature.NewPolicyContext(policy)
		if err != nil {
			log.Fatalf("policy context: %v", err)
		}

		_, err = cp.Image(context.TODO(), pc, dest, src, opts)
		if err != nil {
			log.Fatalf("image copy: %v", err)
		}

	}

}

func provisionMinIO() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	gconf, err := readGenConf()
	if err != nil {
		log.Fatal(err)
	}

	if gconf.MinioPassword == "" {
		gconf.MinioPassword, err = password.Generate(32, 10, 0, false, false)
		if err != nil {
			log.Fatal("generate minio password: %v", err)
		}
		gconf.write()
	}

	uri := "docker.io/minio/minio:RELEASE.2021-01-16T02-19-44Z"
	prepareRegistryContainer(uri, "minio")
	img, err := openContainerFile("minio")
	if err != nil {
		log.Fatal(err)
	}
	defer img.Close()

	for _, x := range mf.Infrastructure.MinIO {

		// ensure the disks are setup

		for i, disk := range x.Disks {
			provisionDisk(x.Host, disk, fmt.Sprintf("minio%d", i))
		}

		// load minio image onto the host

		err = loadContainer(x.Host, img)
		if err != nil {
			log.Fatalf("load minio container: %v", err)
		}

		// create pod

		err = createHostPod("minio", x.Host, false)
		if err != nil {
			log.Fatalf("create minio pod: %v", err)
		}

		sg := specgen.SpecGenerator{}
		sg.Name = "minio-server"
		sg.Pod = "minio"
		sg.Image = "mergetb.io/mars/minio"
		sg.NetNS = specgen.Namespace{
			NSMode: specgen.Host,
		}
		sg.Command = []string{"minio", "server"}
		for i := range x.Disks {
			dest := fmt.Sprintf("/disk%d", i)
			sg.Mounts = append(sg.Mounts, spec.Mount{
				Type:        "bind",
				Destination: dest,
				Source:      fmt.Sprintf("/var/mnt/minio%d", i),
				Options:     []string{"rbind", "rw", "z"},
			})
			sg.Command = append(sg.Command, dest)
		}
		sg.Env = map[string]string{
			"MINIO_ROOT_USER":     "mars",
			"MINIO_ROOT_PASSWORD": gconf.MinioPassword,
		}

		err = createContainer(sg, x.Host)
		if err != nil {
			log.Fatal("create minio container: %v", err)
		}

		// launch the pod

		err = addSporeService("minio", x.Host)
		if err != nil {
			log.Fatalf("add spore minio service: %v", err)
		}

	}

	for {
		ok, state, err := waitForPod("minio", mf.Infrastructure.MinIO[0].Host)
		if err != nil {
			log.Fatal(err)
		}
		if ok {
			break
		}
		log.Printf("wating for %s:minio to come up, currently %s",
			mf.Infrastructure.MinIO[0].Host, state,
		)
		time.Sleep(time.Second * 1)
	}

}

func provisionDisk(host, disk, name string) {

	key := fmt.Sprintf("/disk/%s/%s", host, disk)

	alreadyProvisioned := false
	err := withEtcd(func(c *clientv3.Client) error {
		resp, err := c.Get(context.TODO(), key)
		if err != nil {
			log.Fatalf("disk get: %v", err)
		}
		if len(resp.Kvs) > 0 && string(resp.Kvs[0].Value) == "ext4" {
			// nothing to do
			alreadyProvisioned = true
		}
		return nil
	})
	if !alreadyProvisioned || force {
		log.Printf("Provisioning %s disk on %s", name, host)

		err = addSporeDisk(host, name, disk)
		if err != nil {
			log.Fatal(err)
		}

		err = withEtcd(func(c *clientv3.Client) error {
			_, err = c.Put(context.TODO(), key, "ext4")
			if err != nil {
				log.Fatalf("etcd update: %v", err)
			}
			return nil
		})
	}

}

func provisionImages() error {

	repo := "https://gitlab.com/mergetb/tech/images"
	base := fmt.Sprintf(
		"%s/-/jobs/1068363344/artifacts/raw/debian/build/bullseye/efi", repo)
	img := fmt.Sprintf("%s/debian-bullseye.gz", base)
	initramfs := fmt.Sprintf("%s/collect/sled-initramfs", base)
	kernel := fmt.Sprintf("%s/collect/sled-kernel", base)

	mc, err := marsMinIOClient()
	if err != nil {
		return fmt.Errorf("mars minio client: %v", err)
	}

	// image
	log.Printf("fetching %s", img)
	resp, err := http.Get(img)
	if err != nil {
		return fmt.Errorf("image fetch: %v", err)
	}
	_, err = mc.PutObject(
		context.TODO(),
		"images",
		"bullseye-rootfs",
		resp.Body,
		resp.ContentLength,
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("put image: %v", err)
	}

	// initramfs
	log.Printf("fetching %s", initramfs)
	resp, err = http.Get(initramfs)
	if err != nil {
		return fmt.Errorf("initramfs fetch: %v", err)
	}
	_, err = mc.PutObject(
		context.TODO(),
		"images",
		"bullseye-initramfs",
		resp.Body,
		resp.ContentLength,
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("put initramfs: %v", err)
	}

	// kernel
	log.Printf("fetching %s", kernel)
	resp, err = http.Get(kernel)
	if err != nil {
		return fmt.Errorf("kernel fetch: %v", err)
	}
	_, err = mc.PutObject(
		context.TODO(),
		"images",
		"bullseye-kernel",
		resp.Body,
		resp.ContentLength,
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("put kernel: %v", err)
	}

	return nil

}

// TODO
func provisionCeph() {}

func provisionModel(modelfile string) error {

	f, err := os.Open(modelfile)
	if err != nil {
		return fmt.Errorf("open model: %v", err)
	}

	st, err := f.Stat()
	if err != nil {
		return fmt.Errorf("state model file: %v", err)
	}

	mc, err := marsMinIOClient()
	if err != nil {
		return fmt.Errorf("mars minio client: %v", err)
	}

	_, err = mc.PutObject(
		context.TODO(),
		"sys",
		"tbxir.pbuf",
		f,
		st.Size(),
		minio.PutObjectOptions{ContentType: "application/octet-stream"},
	)
	if err != nil {
		return fmt.Errorf("minio put model: %v", err)
	}

	return nil

}
