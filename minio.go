package main

import (
	"context"
	"fmt"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/minio/minio/pkg/iam/policy"
	"github.com/minio/minio/pkg/madmin"
)

func provisionMinio() error {

	mc, err := marsMinIOClient()
	if err != nil {
		return err
	}

	err = initializeImageBucket(mc)
	if err != nil {
		return err
	}

	err = createBucket("sys", mc)
	if err != nil {
		return err
	}

	err = provisionModel("/etc/tbxir.pbuf")
	if err != nil {
		return err
	}

	return nil

}

func initializeImageBucket(mc *minio.Client) error {

	err := createBucket("images", mc)
	if err != nil {
		return err
	}

	err = createImageBucketPolicy()
	if err != nil {
		return err
	}

	err = createImageBucketReaderUser()
	if err != nil {
		return err
	}

	return nil
}

func createBucket(bucket string, mc *minio.Client) error {

	found, err := mc.BucketExists(context.TODO(), bucket)
	if err != nil {
		return fmt.Errorf("%s bucket exists check: %v", bucket, err)
	}

	if found {
		// nothing to do
		return nil
	}

	err = mc.MakeBucket(context.TODO(), bucket, minio.MakeBucketOptions{})
	if err != nil {
		return fmt.Errorf("make image bucket: %v", err)
	}

	return nil

}

func createImageBucketPolicy() error {

	ma, err := marsMinIOAdminClient()
	if err != nil {
		return fmt.Errorf("new minio admin client: %v", err)
	}

	p := &iampolicy.Policy{
		Version: "2012-10-17",
		Statements: []iampolicy.Statement{
			{
				Actions: iampolicy.ActionSet{"s3:GetObject": {}},
				Effect:  "Allow",
				Resources: iampolicy.ResourceSet{iampolicy.Resource{
					Pattern: "images/*",
				}: {}},
			},
		},
	}

	err = ma.AddCannedPolicy(context.TODO(), "image-read", p)
	if err != nil {
		return fmt.Errorf("add image read policy: %v", err)
	}

	return nil

}

func createImageBucketReaderUser() error {

	ma, err := marsMinIOAdminClient()
	if err != nil {
		return fmt.Errorf("new minio admin client: %v", err)
	}

	// not intended to be an access control at this time, just a well known
	// credential.
	err = ma.AddUser(context.TODO(), "image", "imageread")
	if err != nil {
		return fmt.Errorf("create minio image user: %v", err)
	}

	err = ma.SetPolicy(context.TODO(), "image-read", "image", false)
	if err != nil {
		return fmt.Errorf("set image-read policy to image user: %v", err)
	}

	return nil

}

func marsMinIOClient() (*minio.Client, error) {

	gconf, err := readGenConf()
	if err != nil {
		return nil, err
	}

	return minio.New("minio:9000", &minio.Options{
		Creds: credentials.NewStaticV4(
			"mars",
			gconf.MinioPassword,
			"",
		),
		//TODO Secure: true, // implies ssl
	})

}

func marsMinIOAdminClient() (*madmin.AdminClient, error) {

	gconf, err := readGenConf()
	if err != nil {
		return nil, err
	}

	//TODO use ssl
	ssl := false

	return madmin.New("minio:9000", "mars", gconf.MinioPassword, ssl)

}
