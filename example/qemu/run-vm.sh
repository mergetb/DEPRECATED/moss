#!/bin/bash

if [[ $UID -ne 0 ]]; then
    echo "must be root"
    exit 1
fi

rm -f disk.img
qemu-img create -f qcow2 disk.img 10G

qemu-system-x86_64 \
    -nodefaults \
    -vga std \
    -m 4096 \
    -bios /usr/share/edk2/ovmf/OVMF_CODE.fd \
    -drive file=disk.img,index=0,media=disk \
    -machine q35,accel=kvm \
    -netdev tap,id=net0,ifname=mosst0,script=no -device e1000,netdev=net0
