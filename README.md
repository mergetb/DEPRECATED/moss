# Merge OperationS Server (Moss)

Moss is simple standalone tool for provisioning systems over a network. It has
the following components built in.

- A DHCP server with support for configuring PXE and HTTP boot.
- A DNS server to support pre-boot environments that need to resolve well-known names.
- An HTTP file server for supporting http boot
- A TFTP file server for supporting tftp boot

## TL;DR

```
sudo moss run --nex.interface <ifx>
```

Moss will then download all the artifacts needed to provision your testbed and
start up. Just set your machines to network boot and let Moss take care of the
rest.

**Note, the interface you provide must have an IP addresss, otherwise it cannot be
used as a DHCP/DNS server.**.

Once systems have been provisioned you can ssh into them via
```
ssh -i <workdir>/ssh/id_rsa ops@<ip-addr>
```

On [Installed](#installation) systems `<workdir>` is `/var/moss` if you run
locally it is the directory you launched `moss` from unless you changed it with
the `--moss.workdir` flag.

To see what addresses your systems have

```
$ moss members
mac                  name    ip4                    client
04:70:00:00:00:10            10.0.0.10 (3:59:21)
04:70:00:00:00:11            10.0.0.11 (3:59:23)
04:70:00:00:00:12            10.0.0.12 (3:59:26)
04:70:00:00:00:13            10.0.0.13 (3:59:29)
```

## Installation

In addition to being a quick provisioning asset. Moss is also a long lived
testbed management server.

Place the `moss` binary where you want it (likely /usr/local/bin/moss) and run

```
moss install [args]
```

A quick way to get started is

```
sudo moss install -- --nex.interface <ifx>
sudo systemctl enable moss-server
sudo systemctl start moss-server
```

Where `<ifx>` is the name of the interface on the testbed management network.

Available args are
```shell
$ ./moss -h
Merge OperationS Server

Usage:
  moss [command]

Available Commands:
  fetch       Fetch FCOS and iPXE artifacts
  help        Help about any command
  install     Run the Moss server
  members     List members
  run         Run the Moss server

Flags:
  -h, --help                       help for moss
      --moss.fcos-version string   Fedora CoreOS Version (default "33.20201214.3.1")
      --moss.ipxe-version string   iPXE Version (default "965031063")
      --moss.workdir string        Working directory (default ".")
      --nex.conf string            Place for nex config files
      --nex.domain string          Domain for Nex to resolve (default "local")
      --nex.etcd-ca string         Etcd TLS CA cert file
      --nex.etcd-cert string       Etcd TLS cert file
      --nex.etcd-key string        Etcd TLS key file
      --nex.etcd-port int          Client port of etcd server (default 2379)
      --nex.etcd-server string     Address of etcd server (default "localhost")
      --nex.interface string       Interface to listen on (default "lo")
      --nex.listen string          Address to listen on (default "0.0.0.0:6000")

Use "moss [command] --help" for more information about a command.
```

**In almost all cases you'll need to supply the `--nex.interface` option so Moss
knows what interface to provide DNS/DHCP services over.**

## Provisioning Testbed Systems

Once testbed servers have FCOS loaded, they can be provisioned with testbed
software. In order to do this you need a manifest file name `manifest.yml` in
the moss working directory. Here are a few examples.

### Basic

This is a manifest for a single-server, single-switch testbed.

![](https://gitlab.com/mergetb/tech/mars/-/wikis/uploads/918b81ba6becd0886ba48decb0714130/MARS_-_single.png)

```yaml
hosts:
    apiserver:
        - infraserver
    infrapod:
        - infraserver
    infranet:
        - infraserver
    sled:
        - infraserver
    rally:
        - infraserver
    metal:
        - infraserver
    canopy:
        - cx
infrastructure:
    etcd:
        - host: infraserver
          volume: /dev/sdb
    minio:
        - host: infraserver
          volumes: 
            - /dev/sdc
            - /dev/sdd
    ceph:
        monitors:
            - infraserver
        managers:
            - infraserver
        osds:
            - host: infraserver
              volumes:
                - /dev/sde
                - /dev/sdf
                - /dev/sdg
                - /dev/sdh
```

Here the single server is named `infraserver` and the single switch is named
`cx`.
