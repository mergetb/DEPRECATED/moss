/* Basic Moss Testing Topology
 * ===========================
 *
 * This test topology is for
 *   - 1 operations server
 *   - 1 switch
 *   - 4 testbed nodes
 *
 */

topo = {
    name: "moss_"+Math.random().toString().substr(-6),
    nodes: [
        fedora("ops"),
        ...["x0", "x1", "x2", "x3"].map(x => tbnode(x)),
    ],
    switches: [
        cumulus("cx"),
    ],
    links: [
        v2v("ops", 1, "cx", 1),
        v2v("x0",  1, "cx", 2, { mac: { x0: '04:70:00:00:00:10' }, boot: 20 }),
        v2v("x1",  1, "cx", 3, { mac: { x1: '04:70:00:00:00:11' }, boot: 20 }),
        v2v("x2",  1, "cx", 4, { mac: { x2: '04:70:00:00:00:12' }, boot: 20 }),
        v2v("x3",  1, "cx", 5, { mac: { x3: '04:70:00:00:00:13' }, boot: 20 }),
    ]
};

function v2v(a, ai, b, bi, props={}) {
    lnk = Link(a, ai, b, bi, props);
    lnk.v2v = true;
    return lnk;
}

function tbnode(name) {
    return {
        name: name,
        defaultnic: 'e1000',
        defaultdisktype: { dev: 'sda', bus: 'sata' },
        image: 'netboot',
        os: 'netboot',
        cpu: { cores: 2 },
        memory: { capacity: GB(4) },
        firmware: '/usr/share/edk2/ovmf/OVMF_CODE.fd'
    };
}

function cumulus(name) {
    return {
        name: name,
        image: 'cumulusvx-4.2',
        cpu: { cores: 2 },
        memory: { capacity: GB(2) }
    };
}

function fedora(name) {
    return {
        name: name,
        image: 'fedora-33',
        cpu: { cores: 2 },
        memory: { capacity: GB(2) }
    };
}
