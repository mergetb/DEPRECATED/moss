package main

import (
	"fmt"
	"io/ioutil"

	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Manifest struct {
	Services       Services        `yaml:"services"`
	Infrastructure Infrastructure  `yaml:"infrastructure"`
	Hosts          map[string]Host `yaml:"hosts"`
}

type Services struct {
	Apiserver []string `yaml:"apiserver"`
	Infrapod  []string `yaml:"infrapod"`
	Infranet  []string `yaml:"infranet"`
	Sled      []string `yaml:"sled"`
	Rally     []string `yaml:"rally"`
	Metal     []string `yaml:"metal"`
	Canopy    []string `yaml:"canopy"`
	Frr       []string `yaml:"frr"`
	Nex       []string `yaml:"nex"`
	Tftp      []string `yaml:"tftp"`
}

type Infrastructure struct {
	Etcd        []EtcdConfig  `yaml:"etcd"`
	MinIO       []MinIOConfig `yaml:"minio"`
	Ceph        CephConfig    `yaml:"ceph"`
	Infraserver []string      `yaml:"infraserver"`
}

type EtcdConfig struct {
	Host string `yaml:"host"`
	Disk string `yaml:"disk"`
}

type MinIOConfig struct {
	Host  string   `yaml:"host"`
	Disks []string `yaml:"disks"`
}

type CephConfig struct {
	Monitors []string    `yaml:"monitors"`
	Managers []string    `yaml:"managers"`
	OSDs     []OSDConfig `yaml:"osds"`
}

type OSDConfig struct {
	Host  string   `yaml:"host"`
	Disks []string `yaml:"disks"`
}

type HostKind string

const (
	KindFCOS    HostKind = "FCOS"
	KindCumulus          = "Cumulus"
)

type Host struct {
	Management MangementConfig `yaml:"management"`
	Harbor     HarborConfig    `yaml:"harbor"`
	Kind       HostKind        `yaml:"kind"`
}

type MangementConfig struct {
	Netdev string `yaml:"netdev"`
	Mac    string `yaml:"mac"`
}

type HarborConfig struct {
	TunnelIp string `yaml:"tunnelip"`
	Address  string `yaml:"address"`
	DHCP     bool   `yaml:"dhcp"`
	Netdev   string `yaml:"netdev"`
}

func readManifest() (*Manifest, error) {

	buf, err := ioutil.ReadFile(
		fmt.Sprintf("%s/manifest.yml", viper.GetString("moss.workdir")),
	)
	if err != nil {
		return nil, fmt.Errorf("read manifest: %v", err)
	}

	mf := new(Manifest)

	err = yaml.Unmarshal(buf, mf)
	if err != nil {
		return nil, fmt.Errorf("parse manifest: %v", err)
	}

	return mf, nil

}
