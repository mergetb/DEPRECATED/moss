package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/viper"
	"github.com/takama/daemon"
	"golang.org/x/crypto/ssh"
)

func install(args []string) {

	err := os.MkdirAll("/var/moss/fcos", 0755)
	if err != nil {
		log.Fatalf("error creating /var/moss/fcos dir: %v", err)
	}

	err = os.MkdirAll("/var/moss/ssh", 0755)
	if err != nil {
		log.Fatalf("error creating /var/moss/ssh dir: %v", err)
	}

	err = os.MkdirAll("/var/moss/tftp", 0755)
	if err != nil {
		log.Fatalf("error creating /var/moss/tftp dir: %v", err)
	}

	viper.Set("moss.workdir", "/var/moss")
	fetch()

	args = append([]string{"run", "--moss.workdir", "/var/moss", "--nex.domain", "moss.net"}, args...)

	service, err := daemon.New("moss-server", "The Merge Operations Server", daemon.SystemDaemon)
	if err != nil {
		log.Fatal(err)
	}
	status, err := service.Install(args...)
	if err != nil {
		if err == daemon.ErrAlreadyInstalled {
			service.Stop()
			service.Remove()
			status, err = service.Install(args...)
			if err != nil {
				log.Fatal(err)
			}
		} else {
			log.Fatalf("%s: %v", status, err)
		}
	}

	installKey()

}

func installKey() {

	wd := viper.GetString("moss.workdir")

	_, err := os.Stat(fmt.Sprintf("%s/ssh/id_rsa", wd))
	if err != nil && os.IsNotExist(err) {

		private, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			log.Fatalf("failed to generate private key: %v", err)
		}

		buf := pem.EncodeToMemory(&pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(private),
		})
		err = ioutil.WriteFile(fmt.Sprintf("%s/ssh/id_rsa", wd), buf, 0600)
		if err != nil {
			log.Fatalf("failed to write private key: %v", err)
		}

		public, err := ssh.NewPublicKey(&private.PublicKey)
		if err != nil {
			log.Fatalf("failed to generate public key: %v", err)
		}

		buf = ssh.MarshalAuthorizedKey(public)
		err = ioutil.WriteFile(fmt.Sprintf("%s/ssh/id_rsa.pub", wd), buf, 0644)
		if err != nil {
			log.Fatalf("failed to write public key: %v", err)
		}

	}

}
