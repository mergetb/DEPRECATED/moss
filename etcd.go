package main

import (
	"time"

	"go.etcd.io/etcd/clientv3"
)

func EtcdClient() (*clientv3.Client, error) {

	return clientv3.New(clientv3.Config{
		Endpoints:   []string{"localhost:2379"},
		DialTimeout: 3 * time.Second,
	})

}

func withEtcd(f func(*clientv3.Client) error) error {
	c, err := EtcdClient()
	if err != nil {
		return err
	}
	defer c.Close()

	return f(c)
}
