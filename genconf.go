package main

import (
	"fmt"
	"io/ioutil"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type GeneratedConfig struct {
	MinioPassword string
	BridgeMacs    map[string]string
}

func readGenConf() (*GeneratedConfig, error) {

	gconf := &GeneratedConfig{
		BridgeMacs: make(map[string]string),
	}

	filename := fmt.Sprintf("%s/genconf.yml", viper.GetString("moss.workdir"))

	if fileExists(filename) {
		in, err := ioutil.ReadFile(filename)
		if err != nil {
			return nil, err
		}
		err = yaml.Unmarshal(in, gconf)
		if err != nil {
			return nil, err
		}
	}

	return gconf, nil

}

func (gconf *GeneratedConfig) write() {

	buf, err := yaml.Marshal(gconf)
	if err != nil {
		log.Fatalf("gconf marshal: %v", err)
	}

	filename := fmt.Sprintf("%s/genconf.yml", viper.GetString("moss.workdir"))
	err = ioutil.WriteFile(filename, buf, 0644)
	if err != nil {
		log.Fatalf("gconf write: %v", err)
	}

}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}
