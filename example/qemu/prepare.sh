#!/bin/bash

mkdir -p tftp

if [[ ! -f tftp/snponly.efi ]]; then
    curl -L https://gitlab.com/mergetb/tech/ipxe/-/jobs/965031063/artifacts/raw/src/bin-moss/snponly.efi -o tftp/snponly.efi
fi

mkdir -p http

if [[ ! -f http/kernel ]]; then
    curl -L https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/33.20201214.3.1/x86_64/fedora-coreos-33.20201214.3.1-live-kernel-x86_64 -o http/kernel
fi

if [[ ! -f http/initramfs ]]; then
    curl -L https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/33.20201214.3.1/x86_64/fedora-coreos-33.20201214.3.1-live-initramfs.x86_64.img -o http/initramfs
fi

if [[ ! -f http/rootfs ]]; then
    curl -L https://builds.coreos.fedoraproject.org/prod/streams/stable/builds/33.20201214.3.1/x86_64/fedora-coreos-33.20201214.3.1-live-rootfs.x86_64.img -o http/rootfs
fi

if [[ ! -f ignition/config.ign ]]; then
    podman run -i --rm quay.io/coreos/fcct:release --pretty --strict < ignition/config.yml > ignition/config.ign
fi

if [[ ! -f http/ignition ]]; then
    ln -s ../ignition/config.ign http/ignition
fi
