package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/spf13/viper"
)

func fetch() {
	fetchFCOS()
	fetchIPXE(
		viper.GetString("moss.workdir"),
		viper.GetString("moss.ipxe-version"),
		"moss",
	)
	fetchSled()
	fetchCumulus()
	fetchCanopy()
	fetchNex()
	fetchDance()
}

func fetchCumulus() {

	wd := viper.GetString("moss.workdir")
	outfile := fmt.Sprintf("%s/http/onie-installer-x86_64", wd)
	url := "https://storage.googleapis.com/3p.content.mergetb.dev/cumulus-linux-4.3.0-vx-amd64.bin"

	err := os.MkdirAll(fmt.Sprintf("%s/http", wd), 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s/http", wd)
	}

	fetchFile(url, outfile)

}

func fetchCanopy() {

	wd := viper.GetString("moss.workdir")
	outfile := fmt.Sprintf("%s/http/canopy", wd)
	url := fmt.Sprintf(
		"https://gitlab.com/mergetb/tech/mars/-/jobs/%s/artifacts/raw/build/canopy",
		viper.GetString("moss.mars-version"),
	)

	err := os.MkdirAll(fmt.Sprintf("%s/http", wd), 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s/http", wd)
	}

	fetchFile(url, outfile)

	err = os.Chmod(outfile, 0755)
	if err != nil {
		log.Fatalf("failed to make canopy executable: %v", err)
	}

}

func fetchNex() {

	wd := viper.GetString("moss.workdir")
	outfile := fmt.Sprintf("%s/http/nex-server", wd)
	url := fmt.Sprintf(
		"https://gitlab.com/mergetb/tech/nex/-/jobs/%s/artifacts/raw/build/nex-server",
		viper.GetString("moss.nex-version"),
	)

	err := os.MkdirAll(fmt.Sprintf("%s/http", wd), 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s/http", wd)
	}

	fetchFile(url, outfile)

	err = os.Chmod(outfile, 0755)
	if err != nil {
		log.Fatalf("failed to make canopy executable: %v", err)
	}

}

func fetchDance() {

	wd := viper.GetString("moss.workdir")
	outfile := fmt.Sprintf("%s/http/dance", wd)
	url := fmt.Sprintf(
		"https://gitlab.com/mergetb/tech/mars/-/jobs/%s/artifacts/raw/build/dance",
		viper.GetString("moss.mars-version"),
	)

	err := os.MkdirAll(fmt.Sprintf("%s/http", wd), 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s/http", wd)
	}

	fetchFile(url, outfile)

	err = os.Chmod(outfile, 0755)
	if err != nil {
		log.Fatalf("failed to make dance executable: %v", err)
	}

}

func fetchFCOS() {

	wd := viper.GetString("moss.workdir")
	ver := viper.GetString("moss.fcos-version")
	baseurl := fmt.Sprintf(
		"builds.coreos.fedoraproject.org/prod/streams/stable/builds/%s/x86_64",
		ver,
	)

	// kernel
	outfile := fmt.Sprintf("%s/fcos/kernel", wd)
	url := fmt.Sprintf(
		"https://%s/fedora-coreos-%s-live-kernel-x86_64", baseurl, ver)
	fetchFile(url, outfile)

	// initramfs
	outfile = fmt.Sprintf("%s/fcos/initramfs", wd)
	url = fmt.Sprintf(
		"https://%s/fedora-coreos-%s-live-initramfs.x86_64.img", baseurl, ver)
	fetchFile(url, outfile)

	// rootfs
	outfile = fmt.Sprintf("%s/fcos/rootfs", wd)
	url = fmt.Sprintf(
		"https://%s/fedora-coreos-%s-live-rootfs.x86_64.img", baseurl, ver)
	fetchFile(url, outfile)

}

func fetchSled() {

	wd := viper.GetString("moss.workdir")
	baseurl := "https://gitlab.com/mergetb/tech/sled/-/jobs/1148327617/artifacts/raw/build"

	err := os.MkdirAll(fmt.Sprintf("%s/sled", wd), 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s/sled", wd)
	}

	outfile := fmt.Sprintf("%s/sled/kernel", wd)
	url := fmt.Sprintf("%s/bzImage", baseurl)
	fetchFile(url, outfile)

	outfile = fmt.Sprintf("%s/sled/initramfs", wd)
	url = fmt.Sprintf("%s/sled.cpio", baseurl)
	fetchFile(url, outfile)

}

func fetchIPXE(dest, version, variant string) {

	//wd := viper.GetString("moss.workdir")
	//ver := viper.GetString("moss.ipxe-version")
	baseurl := fmt.Sprintf(
		"https://gitlab.com/mergetb/tech/ipxe/-/jobs/%s/artifacts/raw/src/bin-%s",
		version,
		variant,
	)

	err := os.MkdirAll(fmt.Sprintf("/%s/tftp", dest), 0755)
	if err != nil {
		log.Fatalf("failed to create dir %s/tftp", dest)
	}

	url := fmt.Sprintf("%s/ipxe.efi", baseurl)
	outfile := fmt.Sprintf("/%s/tftp/ipxe.efi", dest)
	fetchFile(url, outfile)

	url = fmt.Sprintf("%s/ipxe.pxe", baseurl)
	outfile = fmt.Sprintf("/%s/tftp/ipxe.pxe", dest)
	fetchFile(url, outfile)

	url = fmt.Sprintf("%s/snponly.efi", baseurl)
	outfile = fmt.Sprintf("/%s/tftp/snponly.efi", dest)
	fetchFile(url, outfile)

}

func fetchFile(url, dst string) {

	_, err := os.Stat(dst)
	if err != nil && os.IsNotExist(err) {

		log.Printf("fetching %s -> %s", url, dst)

		out, err := os.Create(dst)
		if err != nil {
			log.Fatalf("failed to create %s: %v", dst, err)
		}
		defer out.Close()

		resp, err := http.Get(url)
		if err != nil {
			log.Fatalf("failed to fetch %s: %v", url, err)
		}

		_, err = io.Copy(out, resp.Body)
		if err != nil {
			log.Fatalf("failed to copy to %s: %v", dst, err)
		}
	}

}
