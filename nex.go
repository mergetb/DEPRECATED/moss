package main

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/mergetb/tech/nex/nex-server/nexserver"
	"gitlab.com/mergetb/tech/nex/pkg"
)

func runNex() {
	nex.Init()
	go nexserver.Run()
	setupNex()
}

func setupNex() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	ns, err := nex.GetNetworks()
	if err != nil {
		log.Fatalf("get networks: %v", err)
	}

	foundMoss := false
	foundMossStatic := false

	gconf, err := readGenConf()
	if err != nil {
		log.Fatal(err)
	}

	for _, n := range ns {
		if n.Name == "moss" {
			foundMoss = true
			continue
		}
		if n.Name == "moss-static" {
			foundMossStatic = true
		}
	}

	n := &nex.Network{
		Name:        "moss",
		Subnet4:     "10.0.0.0/24",
		Gateways:    []string{"10.0.0.1"},
		Nameservers: []string{"10.0.0.1"},
		Dhcp4Server: "10.0.0.1",
		Domain:      "moss.net",
		MacRange: &nex.AddressRange{
			Begin: "00:00:00:00:00:00",
			End:   "ff:ff:ff:ff:ff:ff",
		},
		Range4: &nex.AddressRange{
			Begin: "10.0.0.10",
			End:   "10.0.0.254",
		},
		Options: []*nex.Option{
			{Number: 67, Value: "snponly.efi"},
			{Number: 239, Value: "http://moss/ztp.sh"},
			//{Number: 114, Value: "http://onie-server/cumulus-installer"},
		},
	}

	if !foundMoss {

		err := nex.AddNetwork(n, true)
		if err != nil {
			log.Fatalf("nex add moss network: %v", err)
		}

		var members []*nex.Member
		for name, host := range mf.Hosts {

			var mac = host.Management.Mac

			if mac == "" {
				continue
			}

			if host.Kind == KindFCOS {
				mac = macgen().String()
				gconf.BridgeMacs[name] = mac
				gconf.write()
			}

			member := &nex.Member{
				Mac:      mac,
				Names:    []string{name},
				HostName: name,
			}

			// add service DNS aliases
			for _, svchost := range mf.Services.Apiserver {
				if svchost == name {
					member.Names = append(member.Names, "apiserver")
				}
			}
			for _, x := range mf.Infrastructure.Etcd {
				if x.Host == name {
					member.Names = append(member.Names, "etcd")
				}
			}
			for _, x := range mf.Infrastructure.MinIO {
				if x.Host == name {
					member.Names = append(member.Names, "minio")
				}
			}

			members = append(members, member)

			if host.Kind == KindFCOS {
				member = &nex.Member{
					Mac:   host.Management.Mac,
					Names: []string{fmt.Sprintf("%s-mgmt", name)},
				}
				members = append(members, member)
			}

		}
		if len(members) > 0 {
			nex.AddMembers(n.Name, members, true)
		}

	}

	if !foundMossStatic {

		n.Name = "moss-static"
		n.MacRange = nil
		n.Range4 = nil
		n.Options = nil

		err = nex.AddNetwork(n, true)
		if err != nil {
			log.Fatalf("nex add moss-static network: %v", err)
		}

		members := []*nex.Member{
			{
				Mac: "00:00:00:00:00:01",
				Names: []string{
					"moss",
					"onie-server",
				},
				Ip4: &nex.Lease{
					Address: "10.0.0.1",
				},
			},
		}

		err = nex.AddMembers(n.Name, members, true)

	}

}

func listMembers() {

	nex.Init()
	ms, err := nex.GetMembers("moss")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Fprint(tw, "mac\tname\tip4\tclient\n")
	for _, m := range ms {
		fmt.Fprintf(tw, "%s\t%s\t%s\t%s\n",
			m.Mac,
			m.Names,
			nex.ShowLease(m.Ip4),
			m.ClientName,
		)
	}
	tw.Flush()

}

func setName(mac string, names []string) {

	nex.Init()

	net := nex.NewNetworkObj(&nex.Network{Name: "moss"})
	err := nex.Read(net)
	if err != nil {
		log.Fatal(err)
	}

	var otx nex.ObjectTx
	otx.Put = append(otx.Put, nex.NewMacIndex(&nex.Member{Mac: mac}))

	_, err = nex.ReadObjects(otx.Put)
	if err != nil {
		log.Fatal(err)
	}

	m := otx.Put[0].(*nex.MacIndex).Member
	for _, name := range names {
		fqdn := strings.ToLower(name + "." + net.Domain)

		exists := false
		for _, x := range m.Names {
			if x == fqdn {
				exists = true
				break
			}
		}
		if !exists {
			m.Names = append(m.Names, fqdn)
		}
	}
	for _, idx := range nex.NewNameIndex(m) {
		otx.Put = append(otx.Put, idx)
	}

	err = nex.RunObjectTx(otx)
	if err != nil {
		log.Fatal(err)
	}

}

func setHostName(mac string, hostname string) {

	nex.Init()

	var otx nex.ObjectTx
	otx.Put = append(otx.Put, nex.NewMacIndex(&nex.Member{Mac: mac}))

	_, err := nex.ReadObjects(otx.Put)
	if err != nil {
		log.Fatal(err)
	}

	m := otx.Put[0].(*nex.MacIndex).Member
	m.HostName = hostname

	err = nex.RunObjectTx(otx)
	if err != nil {
		log.Fatal(err)
	}

}
