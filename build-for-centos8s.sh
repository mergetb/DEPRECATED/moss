#!/bin/bash

podman run \
    -v `pwd`:/code:z \
    -w /code \
    -it registry.gitlab.com/mergetb/devops/builder/centoss-cib \
    ./centos-build.sh
