package main

import (
	"fmt"
	"io"
	"log"
	"os"

	"github.com/pin/tftp"
	"github.com/spf13/viper"
)

func runtftp() {

	readHandler := func(path string, rf io.ReaderFrom) error {

		wkd := viper.GetString("moss.workdir")
		filename := fmt.Sprintf("%s/tftp/%s", wkd, path)

		log.Printf("OPEN: %s", filename)

		f, err := os.Open(filename)
		if err != nil {
			return err
		}

		_, err = rf.ReadFrom(f)
		if err != nil {
			return err
		}

		return nil

	}

	writeHandler := func(path string, wt io.WriterTo) error { return nil }

	server := tftp.NewServer(readHandler, writeHandler)

	log.Fatal(server.ListenAndServe(":69"))

}
