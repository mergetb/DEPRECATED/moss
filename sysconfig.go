package main

import (
	"log"
	"os"
	"os/exec"
	"text/template"

	"github.com/coreos/go-systemd/v22/dbus"
	"github.com/lorenzosaino/go-sysctl"
)

func setupNat(ifx string) {

	setupNftables(ifx)
	setupSysctls()

}

func setupNftables(ifx string) {

	// ensure nftables is installed
	// XXX gross, but dnf does not appear to be a thing with any sort of API
	out, err := exec.Command("dnf", "install", "nftables").CombinedOutput()
	if err != nil {
		log.Fatal("install dnf: %v: %s", err, string(out))
	}

	// setup nftables

	tmpl, err := template.New("nftables").Parse(nftablesTemplate)
	if err != nil {
		log.Fatalf("template create error: %v", err)
	}

	f, err := os.Create("/etc/sysconfig/nftables.conf")
	if err != nil {
		log.Fatalf("failed to create nftables config file: %v", err)
	}

	cfg := NftablesParams{Ifx: ifx}

	err = tmpl.Execute(f, cfg)
	if err != nil {
		log.Fatalf("Failed to create nftables config: %v", err)
	}

	// start and enbale nftables service

	sysd, err := dbus.New()
	if err != nil {
		log.Fatalf("failed to get dbus connection: %v", err)
	}

	_, _, err = sysd.EnableUnitFiles([]string{
		"nftables.service",
	}, false, true)
	if err != nil {
		log.Fatalf("failed to enable nftables service: %v", err)
	}

	_, err = sysd.StartUnit(
		"nftables.service",
		"replace",
		nil,
	)
	if err != nil {
		log.Fatalf("failed to restart nftables service: %v", err)
	}

}

func setupSysctls() {

	// setup sysctl

	tmpl, err := template.New("sysctl").Parse(sysctlTemplate)
	if err != nil {
		log.Fatalf("template create error: %v", err)
	}

	filename := "/etc/sysctl.d/99-moss.conf"
	f, err := os.Create(filename)
	if err != nil {
		log.Fatalf("failed to create %s: %v", filename, err)
	}

	err = tmpl.Execute(f, nil)
	if err != nil {
		log.Fatalf("Failed to create sysctl config: %v", err)
	}

	err = sysctl.LoadConfigAndApply(filename)
	if err != nil {
		log.Fatalf("Failed to apply sysctl")
	}

	// XXX gross, but dracut does not appear to be a thing with any sort of API
	out, err := exec.Command("dracut", "-f").CombinedOutput()
	if err != nil {
		log.Fatal("dracut: %v: %s", err, string(out))
	}

}
