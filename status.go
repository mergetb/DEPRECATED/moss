package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/containers/podman/v3/libpod/define"
	"github.com/containers/podman/v3/pkg/domain/entities"
	"gitlab.com/mergetb/tech/mars/pkg/cli"
)

func showStatus() {

	mf, err := readManifest()
	if err != nil {
		log.Fatal(err)
	}

	showServiceStatus(mf)

}

func showServiceStatus(mf *Manifest) {

	fmt.Printf(cli.Blue("Infrastructure\n"))
	fmt.Printf(cli.Black("========\n"))
	fmt.Fprintf(tw, cli.TableHeader("host", "host kind", "service", "status"))

	for _, x := range mf.Infrastructure.Etcd {
		showPodStatus("etcd", x.Host, mf)
	}

	for _, x := range mf.Infrastructure.MinIO {
		showPodStatus("minio", x.Host, mf)
	}

	tw.Flush()

	fmt.Printf(cli.Blue("Services\n"))
	fmt.Printf(cli.Black("========\n"))
	fmt.Fprintf(tw, cli.TableHeader("host", "host kind", "service", "status"))

	for _, host := range mf.Services.Apiserver {
		showPodStatus("apiserver", host, mf)
	}
	for _, host := range mf.Services.Sled {
		showPodStatus("sled", host, mf)
	}
	for _, host := range mf.Services.Canopy {
		showPodStatus("canopy", host, mf)
	}
	for _, host := range mf.Services.Tftp {
		showPodStatus("tftp", host, mf)
	}
	for _, host := range mf.Services.Frr {
		showPodStatus("frr", host, mf)
	}
	for _, host := range mf.Services.Frr {
		showPodStatus("metal", host, mf)
	}

	tw.Flush()

}

func showPodStatus(name, host string, mf *Manifest) {

	tiHost := cli.TableItem{Item: host}
	tiHostKind := cli.TableItem{Item: "Unknown", Status: cli.ItemStatusError}
	tiService := cli.TableItem{Item: name}
	tiStatus := cli.TableItem{Item: "Unknown", Status: cli.ItemStatusWarn}

	hostinfo, ok := mf.Hosts[host]
	if !ok {
		tiHost.Status = cli.ItemStatusError
	} else {

		tiHostKind.Item = hostinfo.Kind

		// TODO implement checks for other host kinds
		if hostinfo.Kind == KindFCOS {
			tiHostKind.Status = cli.ItemStatusOK
			pir, err := podStatus(name, host)
			if err != nil {
				if strings.Contains(err.Error(), "404") {
					tiHost.Status = cli.ItemStatusOK
					tiStatus.Item = "Not Found"
					tiStatus.Status = cli.ItemStatusError
				} else {
					tiHost.Status = cli.ItemStatusError
				}
			} else {
				tiHost.Status = cli.ItemStatusOK
				updatePodStatus(pir, &tiStatus)
			}
		} else {
			tiHostKind.Status = cli.ItemStatusWarn
		}

	}

	fmt.Fprintf(tw, cli.TableRow(
		tiHost,
		tiHostKind,
		tiService,
		tiStatus,
	))

}

func updatePodStatus(pir entities.PodInspectReport, ti *cli.TableItem) {

	switch pir.State {
	case define.PodStateCreated:
		ti.Item = pir.State
		ti.Status = cli.ItemStatusWarn

	case define.PodStateErrored:
		ti.Item = pir.State
		ti.Status = cli.ItemStatusError

	case define.PodStateExited:
		ti.Item = pir.State
		ti.Status = cli.ItemStatusError

	case define.PodStatePaused:
		ti.Item = pir.State
		ti.Status = cli.ItemStatusError

	case define.PodStateRunning:
		ti.Item = pir.State
		ti.Status = cli.ItemStatusOK

	case define.PodStateDegraded:
		ti.Item = pir.State
		ti.Status = cli.ItemStatusError

	case define.PodStateStopped:
		ti.Status = cli.ItemStatusError
	}

}
