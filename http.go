package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"strings"
	"text/template"

	cnitypes "github.com/containernetworking/cni/pkg/types"
	ignition "github.com/coreos/ignition/v2/config/v3_2/types"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sethvargo/go-password/password"
	"github.com/spf13/viper"
	"gitlab.com/mergetb/tech/nex/pkg"
	"golang.org/x/crypto/bcrypt"
)

func runhttp() {

	fcosdir := fmt.Sprintf("%s/fcos", viper.GetString("moss.workdir"))
	sporedir := fmt.Sprintf("%s/spore", viper.GetString("moss.workdir"))
	basedir := fmt.Sprintf("%s/http", viper.GetString("moss.workdir"))
	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Static("/", basedir)
	e.Static("/fcos", fcosdir)
	e.Static("/spore", sporedir)
	e.GET("/ignition", ignitionHandler)
	e.GET("/ipxe", ipxeHandler)
	e.GET("/nmconfig", nmConfigHandler)
	e.GET("/bridgeconfig", bridgeConfigHandler)
	e.GET("/mgmtifxconfig", mgmtIfxConfigHandler)
	e.GET("/cniconf", cniConfHandler)
	e.GET("/ztp.sh", ztpHandler)

	e.Logger.Fatal(e.Start(":80"))

}

// gleaned from (main, not importable)
//  - https://github.com/containernetworking/plugins/blob/v0.8.0/plugins/main/macvlan/macvlan.go#L43-L48
type NetConf struct {
	cnitypes.NetConf
	Bridge string `json:"bridge"`
	Mode   string `json:"mode,omitempty"`
	MTU    int    `json:"mtu,omitempty"`
}

//derived from with ^^ spliced in
// https://pkg.go.dev/github.com/containernetworking/cni/pkg/types#NetConfList
type NetConfList struct {
	CNIVersion string `json:"cniVersion,omitempty"`

	Name         string     `json:"name,omitempty"`
	DisableCheck bool       `json:"disableCheck,omitempty"`
	Plugins      []*NetConf `json:"plugins,omitempty"`
}

func ignitionHandler(c echo.Context) error {

	wd := viper.GetString("moss.workdir")
	buf, err := ioutil.ReadFile(fmt.Sprintf("%s/ssh/id_rsa.pub", wd))
	if err != nil {
		return fmt.Errorf("read moss ops pubkey: %v", err)
	}
	pubkey := ignition.SSHAuthorizedKey(buf)

	pwfile := fmt.Sprintf("%s/opspw", viper.Get("moss.workdir"))

	var opspw []byte
	_, err = os.Stat(pwfile)
	if err == nil {
		opspw, err = ioutil.ReadFile(pwfile)
		if err != nil {
			return fmt.Errorf("failed to read ops password")
		}
	} else {

		_opspw, err := password.Generate(64, 10, 10, false, false)
		if err != nil {
			return fmt.Errorf("failed to generate ops password: %v", err)
		}
		opspw = []byte(_opspw)

	}
	pwb, err := bcrypt.GenerateFromPassword([]byte(opspw), bcrypt.MinCost)
	if err != nil {
		return fmt.Errorf("failed to hash password: %v", err)
	}

	err = ioutil.WriteFile(
		fmt.Sprintf("%s/opspw", viper.Get("moss.workdir")),
		opspw,
		0644,
	)
	if err != nil {
		return fmt.Errorf("failed to save ops password: %v", err)
	}

	pw := string(pwb)

	remote := strings.Split(c.Request().RemoteAddr, ":")[0]
	mac, err := nex.FindIpv4Mac(net.ParseIP(remote))
	if err != nil {
		return fmt.Errorf("error looking up remote %s: %v", remote, err)
	}

	// podman service

	tmpl, err := template.New("podmanService").Parse(podmanServiceTemplate)
	if err != nil {
		return fmt.Errorf("template create error: %v", err)
	}
	cfg := PodmanServiceParams{Mac: mac.String()}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template exec error: %v", err)
	}
	podmansvc := out.String()

	netdev, _, err := clientIfx(c)
	if err != nil {
		return fmt.Errorf("determine client netdev: %v", err)
	}

	// XXX hardcode
	host := "10.0.0.1"

	sporeSource := fmt.Sprintf("http://%s/spore/spore", host)
	sporeCert := fmt.Sprintf("http://%s/spore/spore.pem", host)
	sporeKey := fmt.Sprintf("http://%s/spore/spore-key.pem", host)
	cniConfig := fmt.Sprintf("http://%s/cniconf", host)
	nmConfig := fmt.Sprintf("http://%s/nmconfig", host)
	bridgeConfig := fmt.Sprintf("http://%s/bridgeconfig", host)
	mgmtIfxConfig := fmt.Sprintf("http://%s/mgmtifxconfig", host)
	sporeMode := 0755
	sporeKeyMode := 0644
	sporeCertMode := 0644
	netconfMode := 0600

	ign := &ignition.Config{
		Ignition: ignition.Ignition{
			Version: "3.2.0",
		},
		Passwd: ignition.Passwd{
			Users: []ignition.PasswdUser{
				{
					Name:              "ops",
					SSHAuthorizedKeys: []ignition.SSHAuthorizedKey{pubkey},
					Groups:            []ignition.Group{"sudo"},
					PasswordHash:      &pw,
				},
			},
		},
		Systemd: ignition.Systemd{
			Units: []ignition.Unit{
				{
					Name:     "podman-api.service",
					Enabled:  &podmanServiceEnabled,
					Contents: &podmansvc,
				},
				{
					Name:     "podman-api-local.service",
					Enabled:  &podmanServiceEnabled,
					Contents: &podmanLocalServiceTemplate,
				},
				{
					Name:     "spore.service",
					Enabled:  &sporeServiceEnabled,
					Contents: &sporeServiceTemplate,
				},
				{
					Name:     "io.podman.dhcp.socket",
					Enabled:  &podmanDhcpEnabled,
					Contents: &podmanDhcpSocketTemplate,
				},
				{
					Name:     "io.podman.dhcp.service",
					Enabled:  &podmanDhcpEnabled,
					Contents: &podmanDhcpServiceTemplate,
				},
			},
		},
		Storage: ignition.Storage{
			Files: []ignition.File{
				{
					Node: ignition.Node{
						Path: "/usr/local/bin/spore",
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &sporeSource,
						},
						Mode: &sporeMode,
					},
				},
				{
					Node: ignition.Node{
						Path: "/var/lib/spore/cert.pem",
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &sporeCert,
						},
						Mode: &sporeCertMode,
					},
				},
				{
					Node: ignition.Node{
						Path: "/var/lib/spore/key.pem",
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &sporeKey,
						},
						Mode: &sporeKeyMode,
					},
				},
				{
					Node: ignition.Node{
						Path: "/etc/cni/net.d/infrapod-control.conflist",
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &cniConfig,
						},
					},
				},
				{
					Node: ignition.Node{
						Path: "/etc/NetworkManager/conf.d/no-imperialism.conf",
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &nmConfig,
						},
					},
				},
				{
					Node: ignition.Node{
						Path: "/etc/NetworkManager/system-connections/bridge.nmconnection",
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &bridgeConfig,
						},
						Mode: &netconfMode,
					},
				},
				{
					Node: ignition.Node{
						Path: fmt.Sprintf(
							"%s/bridge-%s-member.nmconnection",
							"/etc/NetworkManager/system-connections",
							netdev,
						),
					},
					FileEmbedded1: ignition.FileEmbedded1{
						Contents: ignition.Resource{
							Source: &mgmtIfxConfig,
						},
						Mode: &netconfMode,
					},
				},
			},
		},
	}

	return c.JSON(http.StatusOK, ign)

}

func clientIfx(c echo.Context) (string, string, error) {

	remote := strings.Split(c.Request().RemoteAddr, ":")[0]
	remoteIp := net.ParseIP(remote)
	mac, err := nex.FindIpv4Mac(remoteIp)
	if err != nil {
		return "", "", fmt.Errorf("error looking up remote %s: %v", remote, err)
	}

	names, _ := nex.FindIpv4Names(remoteIp)

	netdev := viper.GetString("moss.default-netdev")

	// if there is a manifest with more specific info, use that
	mf, err := readManifest()
	if err == nil {
		for hostname, config := range mf.Hosts {
			for _, name := range names {
				if hostname+"-mgmt.moss.net" == name && config.Management.Netdev != "" {
					netdev = config.Management.Netdev
				}
			}
		}
	}

	return netdev, mac.String(), nil

}

func cniConfHandler(c echo.Context) error {

	cniconf := NetConfList{
		CNIVersion: "0.4.0",
		Name:       "infrapod-control",
		Plugins: []*NetConf{{
			NetConf: cnitypes.NetConf{
				Type: "bridge",
				IPAM: cnitypes.IPAM{
					Type: "dhcp",
				},
			},
			Bridge: "bridge",
		}},
	}

	return c.JSON(http.StatusOK, cniconf)

}

func ipxeHandler(c echo.Context) error {

	tmpl, err := template.New("ipxe").Parse(ipxeTemplate)
	if err != nil {
		return fmt.Errorf("template create error: %v", err)
	}

	netdev, mac, err := clientIfx(c)
	if err != nil {
		return err
	}

	cfg := IpxeParams{
		Id:          "metal",
		InstallDisk: "/dev/sda",
		Kernel:      "fcos/kernel",
		Rootfs:      "fcos/rootfs",
		Initramfs:   "fcos/initramfs",
		Netdev:      netdev,
		Mac:         mac,
		Server:      "10.0.0.1", //using moss does not work due to multi-dhcp/dns - it's a race
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template exec error: %v", err)
	}

	return c.String(http.StatusOK, out.String())

}

func nmConfigHandler(c echo.Context) error {

	return c.String(http.StatusOK, noNmImperialism)

}

func bridgeConfigHandler(c echo.Context) error {

	gconf, err := readGenConf()
	if err != nil {
		return fmt.Errorf("read gconf: %v", err)
	}

	remote := strings.Split(c.Request().RemoteAddr, ":")[0]
	remoteIp := net.ParseIP(remote)
	names, _ := nex.FindIpv4Names(remoteIp)

	var mac string
	for _, x := range names {
		bm, ok := gconf.BridgeMacs[strings.TrimSuffix(x, "-mgmt.moss.net")]
		if ok {
			mac = bm
		}
	}
	if mac == "" {
		return fmt.Errorf("no bridge config found for client")
	}

	cfg := MgmtBrParams{
		Mac: mac,
	}

	tmpl, err := template.New("mgmtbr").Parse(mgmtBrTemplate)
	if err != nil {
		return fmt.Errorf("template error on management br: %v", err)
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("tempalte exec error: %v", err)
	}

	return c.String(http.StatusOK, out.String())

}

func mgmtIfxConfigHandler(c echo.Context) error {

	netdev, _, err := clientIfx(c)
	if err != nil {
		return err
	}

	cfg := MgmtIfxParams{
		Netdev: netdev,
	}

	tmpl, err := template.New("mgmtifx").Parse(mgmtIfxTemplate)
	if err != nil {
		return fmt.Errorf("template error on management ifx: %v", err)
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("template exec error: %v", err)
	}

	return c.String(http.StatusOK, out.String())

}

func ztpHandler(c echo.Context) error {

	gconf, err := readGenConf()
	if err != nil {
		return fmt.Errorf("read gconf: %v", err)
	}

	if gconf.MinioPassword == "" {
		gconf.MinioPassword, err = password.Generate(32, 10, 0, false, false)
		if err != nil {
			log.Fatal("generate minio password: %v", err)
		}
		gconf.write()
	}

	tmpl, err := template.New("ztp").Parse(ztpTemplate)
	if err != nil {
		return fmt.Errorf("ztp template create: %v", err)
	}

	wd := viper.GetString("moss.workdir")
	buf, err := ioutil.ReadFile(fmt.Sprintf("%s/ssh/id_rsa.pub", wd))
	if err != nil {
		return fmt.Errorf("read moss ops pubkey: %v", err)
	}
	pubkey := strings.TrimSuffix(string(buf), "\n")

	buf, err = ioutil.ReadFile(fmt.Sprintf("%s/spore/spore.pem", wd))
	if err != nil {
		return fmt.Errorf("read spore tls cert: %v", err)
	}
	tlscert := strings.TrimSuffix(string(buf), "\n")

	buf, err = ioutil.ReadFile(fmt.Sprintf("%s/spore/spore-key.pem", wd))
	if err != nil {
		return fmt.Errorf("read spore tls key: %v", err)
	}
	tlskey := strings.TrimSuffix(string(buf), "\n")

	cfg := ZtpParams{
		Pubkey:        pubkey,
		TLSCert:       tlscert,
		TLSKey:        tlskey,
		MinioPassword: gconf.MinioPassword,
		//License: TODO
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, cfg)
	if err != nil {
		return fmt.Errorf("ztp template exec error: %v", err)
	}

	return c.String(http.StatusOK, out.String())

}
