#!/bin/bash

if [[ $UID -ne 0 ]]; then
    echo "must be root"
    exit 1
fi

ip link del mossbr &> /dev/null
ip link del mosst0 &> /dev/null

ip link add mossbr type bridge
ip tuntap add dev mosst0 mode tap

ip link set dev mosst0 master mossbr
ip link set up dev mossbr
ip link set up dev mosst0

ip addr add 10.0.0.1/24 dev mossbr

firewall-cmd --add-interface=mossbr --zone=trusted &> /dev/null
firewall-cmd --add-interface=mosst0 --zone=trusted &> /dev/null
