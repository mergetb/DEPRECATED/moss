package main

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io/ioutil"

	//log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"

	"gitlab.com/mergetb/ops/moss/pkg/spore"
)

func setSporeHostname(host string) error {

	conn, cli, err := sporeClient(host)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = cli.SetHostname(context.TODO(), &spore.SetHostnameRequest{Hostname: host})
	if err != nil {
		return fmt.Errorf("spore call: %v", err)
	}

	return nil

}

func addSporeService(svc, host string) error {

	conn, cli, err := sporeClient(host)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = cli.AddService(context.TODO(), &spore.AddServiceRequest{Name: svc})
	if err != nil {
		return fmt.Errorf("spore call: %v", err)
	}

	return nil

}

func addSporeDisk(host, name, dev string) error {

	conn, cli, err := sporeClient(host)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = cli.AddDisk(context.TODO(), &spore.AddDiskRequest{
		Name: name,
		Dev:  dev,
	})
	if err != nil {
		return fmt.Errorf("spore call: %v", err)
	}

	return nil

}

func addSporeDir(host, path string, uid, gid, mode uint32) error {

	conn, cli, err := sporeClient(host)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = cli.AddDirectory(context.TODO(), &spore.AddDirectoryRequest{
		Path: path,
		Uid:  uint32(uid),
		Gid:  uint32(gid),
		Mode: uint32(mode),
	})
	if err != nil {
		return fmt.Errorf("spore call: %v", err)
	}

	return nil

}

func addSporeFile(host, path string, uid, gid, mode uint32, content []byte) error {

	conn, cli, err := sporeClient(host)
	if err != nil {
		return err
	}
	defer conn.Close()

	_, err = cli.AddFile(context.TODO(), &spore.AddFileRequest{
		Path:    path,
		Uid:     uint32(uid),
		Gid:     uint32(gid),
		Mode:    uint32(mode),
		Content: content,
	})
	if err != nil {
		return fmt.Errorf("spore call: %v", err)
	}

	return nil

}

func sporeClient(host string) (*grpc.ClientConn, spore.SporeClient, error) {

	wkd := viper.GetString("moss.workdir")
	cert := fmt.Sprintf("%s/spore/spore.pem", wkd)
	endpoint := fmt.Sprintf("%s:6000", host)

	var creds credentials.TransportCredentials
	var err error

	creds, err = tlsConfig(cert, endpoint)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to read cert file: %v", err)
	}

	conn, err := grpc.Dial(endpoint,
		grpc.WithTransportCredentials(creds),
		grpc.WithDefaultCallOptions(
			grpc.MaxCallSendMsgSize(1024*1024*1024*4), //4GB
		),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("spore connection failed: %v", err)
	}
	client := spore.NewSporeClient(conn)

	return conn, client, nil

}

func tlsConfig(cert, endpoint string) (credentials.TransportCredentials, error) {

	b, err := ioutil.ReadFile(cert)
	if err != nil {
		return nil, err
	}
	cp := x509.NewCertPool()
	if !cp.AppendCertsFromPEM(b) {
		return nil, fmt.Errorf("credentials: failed to append certificates")
	}
	return credentials.NewTLS(&tls.Config{
		ServerName:         endpoint,
		RootCAs:            cp,
		InsecureSkipVerify: true, //TODO remove this eventually
	}), nil

}
