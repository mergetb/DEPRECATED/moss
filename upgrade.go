package main

import (
	"log"
	"strings"
)

var upgraders = map[string]func(){
	"apiserver": upgradeApiserver,
	"infrapod":  upgradeInfrapod,
	"infranet":  upgradeInfranet,
	"sled":      upgradeSled,
	"rally":     upgradeRally,
	"metal":     upgradeMetal,
	"canopy":    upgradeCanopy,
}

func upgradeApiserver() {}
func upgradeInfrapod()  {}
func upgradeInfranet()  {}
func upgradeSled()      {}
func upgradeRally()     {}
func upgradeMetal()     {}
func upgradeCanopy()    {}

func upgraderList() []string {
	var ls []string
	for x := range upgraders {
		ls = append(ls, x)
	}
	return ls
}

func upgrade(args []string) {

	if len(args) > 0 {
		// if the caller has asked for a specific set of updaters use those
		for _, arg := range args {
			upgrader, ok := upgraders[arg]
			if !ok {
				log.Fatalf(
					"%s is not a recognized service, must be one of %s",
					arg,
					strings.Join(upgraderList(), ""),
				)
			}
			upgrader()
		}
	} else {
		// otherwise run all updaters
		for _, upgrader := range upgraders {
			upgrader()
		}
	}

}
